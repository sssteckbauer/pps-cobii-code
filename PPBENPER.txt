000100**************************************************************/   30930413
000200*  PROGRAM: PPBENPER                                         */   30930413
000300*  RELEASE: ____0413____  SERVICE REQUEST(S): ____3093____   */   30930413
000400*  NAME:____JIM WILLIAMS__CREATION DATE:      __05/23/89__   */   30930413
000500*  DESCRIPTION:                                              */   30930413
000600*  - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II. */   30930413
000700**************************************************************/   30930413
000800**************************************************************/   14240278
000801*  PROGRAM:  PPBENPER                                        */   14240278
000802*  REL#:  0278   REF: NONE  SERVICE REQUESTS:   ____1424____ */   14240278
000803*  NAME ___BMB_________   MODIFICATION DATE ____01/26/87_____*/   14240278
000804*  DESCRIPTION                                               */   14240278
000805*      THIS CODE WAS REMOVED FROM USER40 AND PLACED IN       */   14240278
000806*   THIS STAND ALONE CALLED MODULE.                          */   14240278
000807*                                                            */   14240278
000808**************************************************************/   14240278
000809 IDENTIFICATION DIVISION.                                         PPBENPER
000810 PROGRAM-ID. PPBENPER.                                            PPBENPER
000820*SOURCE-COMPUTER.                          COPY 'CPOTXUCS'.       30930413
000830*OBJECT-COMPUTER.                          COPY 'CPOTXOBJ'.       30930413
000840 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPBENPER
000850 AUTHOR.                                                          PPBENPER
000860******************************************************************PPBENPER
000870******************************************************************PPBENPER
000880*NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE*PPBENPER
000890******************************************************************PPBENPER
000900******************************************************************PPBENPER
001000* CONSULTANT SERVICES PROVIDED BY:                               *PPBENPER
001100*                 BRUCE M. BRISCOE                               *PPBENPER
001200*                 CREATIVELY APPLIED TECHNOLOGIES, INC.          *PPBENPER
001300*                 7548 BRAIDBURN AVE.                            *PPBENPER
001400*                 NEWARK, CA 94560                               *PPBENPER
001500*                  (415) 791-7636                                *PPBENPER
001600******************************************************************PPBENPER
001700******************************************************************PPBENPER
001800*NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE*PPBENPER
001900******************************************************************PPBENPER
002000******************************************************************PPBENPER
002100 DATE-WRITTEN.                                                    PPBENPER
002200 DATE-COMPILED.                                                   PPBENPER
002300*REMARKS.                                                         30930413
002400******************************************************************PPBENPER
002500*                                                                *PPBENPER
002600*  THIS MODULE PERFORMS PERS RETIREMENT DEDUCTION CALCULATIONS.  *PPBENPER
002700*                                                                *PPBENPER
002800******************************************************************PPBENPER
002900 ENVIRONMENT DIVISION.                                            PPBENPER
003000 CONFIGURATION SECTION.                                           PPBENPER
003100 SOURCE-COMPUTER.                          COPY 'CPOTXUCS'.       30930413
003200 OBJECT-COMPUTER.                          COPY 'CPOTXOBJ'.       30930413
003300 SPECIAL-NAMES.                                                   PPBENPER
003400 INPUT-OUTPUT SECTION.                                            PPBENPER
003500 FILE-CONTROL.                                                    PPBENPER
003600     SKIP3                                                        PPBENPER
003700******************************************************************PPBENPER
003800*                                                                *PPBENPER
003900*                         DATA DIVISION                          *PPBENPER
004000*                                                                *PPBENPER
004100******************************************************************PPBENPER
004200 DATA DIVISION.                                                   PPBENPER
004300 FILE SECTION.                                                    PPBENPER
004400     SKIP1                                                        PPBENPER
004500     EJECT                                                        PPBENPER
004600******************************************************************PPBENPER
004700*                                                                *PPBENPER
004800*                    WORKING-STORAGE SECTION                     *PPBENPER
004900*                                                                *PPBENPER
005000******************************************************************PPBENPER
005100 WORKING-STORAGE SECTION.                                         PPBENPER
005200     SKIP1                                                        PPBENPER
005300 01  WS-ID                        PIC X(37)                       PPBENPER
005400     VALUE 'PPBENPER WORKING-STORAGE BEGINS HERE'.                PPBENPER
005500     SKIP3                                                        PPBENPER
005600 01  SUBSCRIPTS.                                                  PPBENPER
005700     05  VALUE-SUBSCRIPT          PIC 9(02) COMP.                 PPBENPER
005800     05  PARM-SUBSCRIPT           PIC 9(02) COMP.                 PPBENPER
005900     05  DED-SUBSCRIPT            PIC 9(02) COMP.                 PPBENPER
006000     SKIP3                                                        PPBENPER
006100 01  FLAGS-AND-SWITCHES.                                          PPBENPER
006200     05  PROGRAM-STATUS-FLAG         PIC X(01)  VALUE '0'.        PPBENPER
006300         88  PROGRAM-STATUS-NORMAL              VALUE '0'.        PPBENPER
006400         88  PROGRAM-STATUS-EXITING             VALUE '1'.        PPBENPER
006500     05  SYSPARM-TABLE-LOADED-SW     PIC X(01)  VALUE 'N'.        PPBENPER
006600         88  SYSPARM-TABLE-LOADED               VALUE 'Y'.        PPBENPER
006700     SKIP1                                                        PPBENPER
006800 01  CONSTANT-VALUES.                                             PPBENPER
006900     05  CONSTANT-NORMAL             PIC X(01)  VALUE '0'.        PPBENPER
007000     05  CONSTANT-ABORT              PIC X(01)  VALUE '1'.        PPBENPER
007100     05  CONSTANT-EXIT-PROGRAM       PIC X(01)  VALUE '1'.        PPBENPER
007200     05  CONSTANT-7                  PIC S9(4)  VALUE +7   COMP.  PPBENPER
007300*                                                                 PPBENPER
007310 01  SYSTEM-PARAMETER-INTERFACE.                                  30930413
007320                                     COPY 'CPWSXSPI'.             PPBENPER
007330*                                                                 PPBENPER
007340*    USED FOR THE STORAGE OF THE "OTHER" 7 PARM NUMS & VALUES     PPBENPER
007350*                                                                 PPBENPER
007360     05  ADDL-PARMS-AREA             PIC X(49).                   PPBENPER
007370     05  ADDL-PARMS REDEFINES                                     PPBENPER
007380         ADDL-PARMS-AREA.                                         PPBENPER
007390         10  ADDL-PARM-TABLE             OCCURS 7 TIMES.          PPBENPER
007400             15  ADDL-PARM-NO        PIC S9(3)      COMP-3.       PPBENPER
007500             15  ADDL-PARM-VALU      PIC S9(5)V9(4) COMP-3.       PPBENPER
007600*                                                                 PPBENPER
007700*    USED FOR THE CALLING SYSTEM PARAMETERS OUTPUT                PPBENPER
007800*                                                                 PPBENPER
007900     05  PARM-NO-236                 PIC S9(3)  VALUE +236 COMP-3.PPBENPER
008000     05  PARM-VALUE-236              PIC S9(5)V9(4)               PPBENPER
008100                                                VALUE +0   COMP-3.PPBENPER
008200     05  PARM-NOS-N-VALUES.                                       PPBENPER
008300         10  PARM-NO-237             PIC S9(3)  VALUE +237 COMP-3.PPBENPER
008400         10  PARM-VALUE-237          PIC S9(5)V9(4)               PPBENPER
008500                                                VALUE +0   COMP-3.PPBENPER
008600         10  PARM-NO-238             PIC S9(3)  VALUE +238 COMP-3.PPBENPER
008700         10  PARM-VALUE-238          PIC S9(5)V9(4)               PPBENPER
008800                                                VALUE +0   COMP-3.PPBENPER
008900         10  PARM-NO-239             PIC S9(3)  VALUE +239 COMP-3.PPBENPER
009000         10  PARM-VALUE-239          PIC S9(5)V9(4)               PPBENPER
009100                                                VALUE +0   COMP-3.PPBENPER
009200         10  PARM-NO-240             PIC S9(3)  VALUE +240 COMP-3.PPBENPER
009300         10  PARM-VALUE-240          PIC S9(5)V9(4)               PPBENPER
009400                                                VALUE +0   COMP-3.PPBENPER
009500         10  PARM-NO-241             PIC S9(3)  VALUE +241 COMP-3.PPBENPER
009600         10  PARM-VALUE-241          PIC S9(5)V9(4)               PPBENPER
009700                                                VALUE +0   COMP-3.PPBENPER
009800         10  PARM-NO-242             PIC S9(3)  VALUE +242 COMP-3.PPBENPER
009900         10  PARM-VALUE-242          PIC S9(5)V9(4)               PPBENPER
010000                                                VALUE +0   COMP-3.PPBENPER
010100         10  PARM-NO-243             PIC S9(3)  VALUE +243 COMP-3.PPBENPER
010200         10  PARM-VALUE-243          PIC S9(5)V9(4)               PPBENPER
010300                                                VALUE +0   COMP-3.PPBENPER
010400*                                                                 PPBENPER
010500     SKIP3                                                        PPBENPER
010600 01  FILLER                       PIC X(37)                       PPBENPER
010700     VALUE 'PPBENPER WORKING-STORAGE ENDS HERE'.                  PPBENPER
010800     EJECT                                                        PPBENPER
010900******************************************************************PPBENPER
011000*                                                                *PPBENPER
011100*                        LINKAGE SECTION                         *PPBENPER
011200*                                                                *PPBENPER
011300******************************************************************PPBENPER
011400 LINKAGE SECTION.                                                 PPBENPER
011500     SKIP1                                                        PPBENPER
011600 01  PPBENPER-INTERFACE.          COPY 'CPLNKPER'.                PPBENPER
011700     EJECT                                                        PPBENPER
011800******************************************************************PPBENPER
011900*                                                                *PPBENPER
012000*                       PROCEDURE DIVISION                       *PPBENPER
012100*                                                                *PPBENPER
012200******************************************************************PPBENPER
012300     SKIP1                                                        PPBENPER
012400 PROCEDURE DIVISION USING PPBENPER-INTERFACE.                     PPBENPER
012500     SKIP1                                                        PPBENPER
012600 A000-MAINLINE SECTION.                                           PPBENPER
012700     SKIP2                                                        PPBENPER
012800     MOVE CONSTANT-NORMAL TO PROGRAM-STATUS-FLAG.                 PPBENPER
012900     SKIP1                                                        PPBENPER
013000     PERFORM B010-INITIALIZE                                      PPBENPER
013100        THRU B019-INITIALIZE-EXIT.                                PPBENPER
013200     SKIP1                                                        PPBENPER
013300     IF PROGRAM-STATUS-NORMAL                                     PPBENPER
013400*    THEN                                                         30930413
013500         IF NOT KPER-RETIRE-PERS                                  PPBENPER
013600             NEXT SENTENCE                                        PPBENPER
013700         ELSE                                                     PPBENPER
013800             PERFORM C010-SET-UP-CURRENT-RATE                     PPBENPER
013900                THRU C019-SET-UP-CURRENT-RATE-EXIT                PPBENPER
014000*            THEN                                                 30930413
014100             IF KPER-RETIREMENT-GROSS = ZEROS                     PPBENPER
014200                 NEXT SENTENCE                                    PPBENPER
014300             ELSE                                                 PPBENPER
014400                 PERFORM D010-SET-RET-GROSS-ADJ                   PPBENPER
014500                    THRU D019-SET-RET-GROSS-ADJ-EXIT              PPBENPER
014600                                                                  PPBENPER
014700*                *********************************************    PPBENPER
014800*                DO CALCS FOR EACH - THE CURR MO RETIRE GROSS     PPBENPER
014900*                   THRU 4 MO OLD RETIRE GROSSES                  PPBENPER
015000*                *********************************************    PPBENPER
015100*                                                                 PPBENPER
015200                 PERFORM E010-CALC-PORTION-DEDUCTION              PPBENPER
015300                    THRU E019-CALC-PORTION-DED-EXIT               PPBENPER
015400                         VARYING DED-SUBSCRIPT FROM 1 BY 1        PPBENPER
015500                           UNTIL DED-SUBSCRIPT > 5                PPBENPER
015600     ELSE                                                         PPBENPER
015700         NEXT SENTENCE.                                           PPBENPER
015800     SKIP3                                                        PPBENPER
015900 A009-GOBACK.                                                     PPBENPER
016000     GOBACK.                                                      PPBENPER
016100     EJECT                                                        PPBENPER
016200 B000-INITIALIZATION SECTION.                                     PPBENPER
016300     SKIP2                                                        PPBENPER
016400 B010-INITIALIZE.                                                 PPBENPER
016500     SKIP1                                                        PPBENPER
016600     MOVE CONSTANT-NORMAL TO KPER-RETURN-STATUS-FLAG.             PPBENPER
016700     SKIP1                                                        PPBENPER
016800     IF NOT SYSPARM-TABLE-LOADED                                  PPBENPER
016900         PERFORM B070-LOAD-SYS-PARAMETERS                         PPBENPER
017000            THRU B079-LOAD-SYS-PARAMETERS-EXIT.                   PPBENPER
017100*                                                                 PPBENPER
017200******************************************************************PPBENPER
017300*      SET NUMERIC RETURN FIELDS TO ZERO                         *PPBENPER
017400******************************************************************PPBENPER
017500*                                                                 PPBENPER
017600     MOVE ZERO TO KPER-CUR-MO-RETIRE-RATE                         PPBENPER
017700                  KPER-PERS-COMP-GROSS                            PPBENPER
017800                  KPER-PERS-EXEMPT-AMOUNT                         PPBENPER
017900                  KPER-CUR-MO-RETIRE-GROSS-ADJ                    PPBENPER
018000                  KPER-CUR-MO-PORT-DED                            PPBENPER
018100                  KPER-1-MO-PORT-DED                              PPBENPER
018200                  KPER-2-MO-PORT-DED                              PPBENPER
018300                  KPER-3-MO-PORT-DED                              PPBENPER
018400                  KPER-4-MO-PORT-DED                              PPBENPER
018500                  KPER-TOT-PERS-RETIRE-DEDS.                      PPBENPER
018600     SKIP1                                                        PPBENPER
018700     PERFORM B030-VALIDATE-LOOKUP-ARGS                            PPBENPER
018800        THRU B039-VALIDATE-LOOKUP-ARGS-EXIT.                      PPBENPER
018900     SKIP1                                                        PPBENPER
019000     PERFORM B050-VALIDATE-ENROLLMENT                             PPBENPER
019100        THRU B059-VALIDATE-ENROLLMENT-EXIT.                       PPBENPER
019200     SKIP3                                                        PPBENPER
019300 B019-INITIALIZE-EXIT.                                            PPBENPER
019400     EXIT.                                                        PPBENPER
019500     SKIP3                                                        PPBENPER
019600 B030-VALIDATE-LOOKUP-ARGS.                                       PPBENPER
019700     SKIP1                                                        PPBENPER
019800     MOVE '0' TO KPER-INVALID-LOOKUP-ARG-FLAG.                    PPBENPER
019900     SKIP1                                                        PPBENPER
020000     IF NOT KPER-VALID-PAY-SCHED                                  PPBENPER
020100         MOVE '1' TO KPER-INVALID-LOOKUP-ARG-FLAG.                PPBENPER
020200     SKIP1                                                        PPBENPER
020300     IF KPER-INVALID-LOOKUP-ARG                                   PPBENPER
020400         MOVE '1' TO PROGRAM-STATUS-FLAG.                         PPBENPER
020500     SKIP3                                                        PPBENPER
020600 B039-VALIDATE-LOOKUP-ARGS-EXIT.                                  PPBENPER
020700     EXIT.                                                        PPBENPER
020800     SKIP3                                                        PPBENPER
020900 B050-VALIDATE-ENROLLMENT.                                        PPBENPER
021000     SKIP1                                                        PPBENPER
021100     MOVE '0' TO KPER-NOT-ENROLLED-IN-PER-FLAG.                   PPBENPER
021200     IF KPER-RETIRE-PERS                                          PPBENPER
021300         NEXT SENTENCE                                            PPBENPER
021400     ELSE                                                         PPBENPER
021500         MOVE '1' TO KPER-NOT-ENROLLED-IN-PER-FLAG.               PPBENPER
021600     SKIP1                                                        PPBENPER
021700     IF KPER-NOT-ENROLLED-IN-PER                                  PPBENPER
021800         MOVE '1' TO PROGRAM-STATUS-FLAG.                         PPBENPER
021900     SKIP3                                                        PPBENPER
022000 B059-VALIDATE-ENROLLMENT-EXIT.                                   PPBENPER
022100     EXIT.                                                        PPBENPER
022200     SKIP3                                                        PPBENPER
022300 B070-LOAD-SYS-PARAMETERS.                                        PPBENPER
022400     SKIP1                                                        PPBENPER
022500*    *************************************************************PPBENPER
022600*    LOAD IN THE FIRST PARM NUMBER AND THE NUMBER OF PARMS WANTED PPBENPER
022700*    *************************************************************PPBENPER
022800*                                                                 PPBENPER
022900     MOVE PARM-NO-236  TO XSPI-PARM-NO.                           PPBENPER
023000     MOVE CONSTANT-7   TO XSPI-TABLE-SIZE.                        PPBENPER
023100*                                                                 PPBENPER
023200*    *************************************************************PPBENPER
023300*    LOAD IN THE OTHER 7 PARM NUMBERS                             PPBENPER
023400*    *************************************************************PPBENPER
023500*                                                                 PPBENPER
023600     MOVE PARM-NOS-N-VALUES TO ADDL-PARMS-AREA.                   PPBENPER
023700     PERFORM R010-CALL-PPPRMUTL                                   PPBENPER
023800        THRU R019-CALL-PPPRMUTL-EXIT.                             PPBENPER
023900     IF XSPI-PARM-VALUE = ZEROS                                   PPBENPER
024000         MOVE '1' TO PROGRAM-STATUS-FLAG                          PPBENPER
024100     ELSE                                                         PPBENPER
024200         MOVE 'Y' TO SYSPARM-TABLE-LOADED-SW                      PPBENPER
024300*                                                                 PPBENPER
024400*        *********************************************************PPBENPER
024500*        SAVE THE PARM VALUE FOR 236                              PPBENPER
024600*        *********************************************************PPBENPER
024700*                                                                 PPBENPER
024800         MOVE XSPI-PARM-VALUE TO PARM-VALUE-236                   PPBENPER
024900*                                                                 PPBENPER
025000*        *********************************************************PPBENPER
025100*        SAVE THE OTHER 7 PARM VALUES                             PPBENPER
025200*        *********************************************************PPBENPER
025300*                                                                 PPBENPER
025400         MOVE ADDL-PARMS-AREA TO PARM-NOS-N-VALUES.               PPBENPER
025500     SKIP1                                                        PPBENPER
025600 B079-LOAD-SYS-PARAMETERS-EXIT.                                   PPBENPER
025700     EXIT.                                                        PPBENPER
025800     EJECT                                                        PPBENPER
025900 C000-SET-UP-RATE SECTION.                                        PPBENPER
026000* ****************************************************************PPBENPER
026100*    THIS PARAGRAPH ESTABLISHES THE VALUE OF THE CURRENT MONTH    PPBENPER
026200*     RETIREMENT RATE                                             PPBENPER
026300* ****************************************************************PPBENPER
026400     SKIP2                                                        PPBENPER
026500 C010-SET-UP-CURRENT-RATE.                                        PPBENPER
026600     SKIP1                                                        PPBENPER
026700* ****************************************************************PPBENPER
026800*    SET THE CURRENT MONTH RETIREMENT RATE TO EITHER PERS         PPBENPER
026900*     RETIREMENT RATE WITH FICA OR WITHOUT FICA                   PPBENPER
027000* ****************************************************************PPBENPER
027100     IF KPER-FICA-ELIG                                            PPBENPER
027200         MOVE PARM-VALUE-242 TO KPER-CUR-MO-RETIRE-RATE           PPBENPER
027300     ELSE                                                         PPBENPER
027400         MOVE PARM-VALUE-243 TO KPER-CUR-MO-RETIRE-RATE.          PPBENPER
027500     SKIP3                                                        PPBENPER
027600 C019-SET-UP-CURRENT-RATE-EXIT.                                   PPBENPER
027700     EXIT.                                                        PPBENPER
027800     EJECT                                                        PPBENPER
027900 D000-ADJUST-GROSS SECTION.                                       PPBENPER
028000     SKIP3                                                        PPBENPER
028100 D010-SET-RET-GROSS-ADJ.                                          PPBENPER
028200     SKIP2                                                        PPBENPER
028300     IF KPER-FICA-ELIG                                            PPBENPER
028400         PERFORM D030-ADJUST-RET-GROSS                            PPBENPER
028500            THRU D039-ADJUST-RET-GROSS-EXIT                       PPBENPER
028600     ELSE                                                         PPBENPER
028700         MOVE KPER-CUR-MO-RETIRE-GROSS                            PPBENPER
028800           TO KPER-CUR-MO-RETIRE-GROSS-ADJ.                       PPBENPER
028900     SKIP2                                                        PPBENPER
029000 D019-SET-RET-GROSS-ADJ-EXIT.                                     PPBENPER
029100     EXIT.                                                        PPBENPER
029200     SKIP2                                                        PPBENPER
029300 D030-ADJUST-RET-GROSS.                                           PPBENPER
029400* ****************************************************************PPBENPER
029500*    THIS PARAGRAPH CALCULATES THE CURRENT RETIREMENT MONTH       PPBENPER
029600*     GROSS ADJUSTED AMOUNT                                       PPBENPER
029700* ****************************************************************PPBENPER
029800     SKIP2                                                        PPBENPER
029900     PERFORM D060-SET-UP-CMP-EXMPT-AMTS                           PPBENPER
030000        THRU D069-SET-UP-CMP-EXIT.                                PPBENPER
030100     SKIP1                                                        PPBENPER
030200     IF KPER-CUR-MO-RETIRE-GROSS NOT = ZEROS                      PPBENPER
030300*                                                                 PPBENPER
030400         IF (KPER-CUR-MO-RETIRE-GROSS > ZERO                      PPBENPER
030500                       AND                                        PPBENPER
030600             KPER-CUR-MO-RETIRE-GROSS < KPER-PERS-COMP-GROSS)     PPBENPER
030700*                                                                 PPBENPER
030800                       OR                                         PPBENPER
030900*                                                                 PPBENPER
031000            (KPER-CUR-MO-RETIRE-GROSS < ZERO                      PPBENPER
031100                       AND                                        PPBENPER
031200             KPER-CUR-MO-RETIRE-GROSS >                           PPBENPER
031300                (KPER-PERS-COMP-GROSS * -1))                      PPBENPER
031400*                                                                 PPBENPER
031500             COMPUTE KPER-CUR-MO-RETIRE-GROSS-ADJ ROUNDED =       PPBENPER
031600                    ((KPER-CUR-MO-RETIRE-GROSS * 2) / 3)          PPBENPER
031700         ELSE                                                     PPBENPER
031800             COMPUTE KPER-CUR-MO-RETIRE-GROSS-ADJ ROUNDED =       PPBENPER
031900                     KPER-CUR-MO-RETIRE-GROSS -                   PPBENPER
032000                     KPER-PERS-EXEMPT-AMOUNT.                     PPBENPER
032100     SKIP3                                                        PPBENPER
032200 D039-ADJUST-RET-GROSS-EXIT.                                      PPBENPER
032300     EXIT.                                                        PPBENPER
032400     SKIP3                                                        PPBENPER
032500 D060-SET-UP-CMP-EXMPT-AMTS.                                      PPBENPER
032600* ****************************************************************PPBENPER
032700*    THIS PARAGRAPH ESTABLISHES THE PERS COMPARISON GROSS AND     PPBENPER
032800*     PERS EXEMPTION                                              PPBENPER
032900* ****************************************************************PPBENPER
033000     SKIP1                                                        PPBENPER
033100     IF KPER-PAY-BIWEEKLY                                         PPBENPER
033200         MOVE PARM-VALUE-238 TO KPER-PERS-COMP-GROSS              PPBENPER
033300         MOVE PARM-VALUE-241 TO KPER-PERS-EXEMPT-AMOUNT           PPBENPER
033400     ELSE                                                         PPBENPER
033500         IF KPER-PAY-SEMI-MONTHLY                                 PPBENPER
033600             MOVE PARM-VALUE-237 TO KPER-PERS-COMP-GROSS          PPBENPER
033700             MOVE PARM-VALUE-240 TO KPER-PERS-EXEMPT-AMOUNT       PPBENPER
033800         ELSE                                                     PPBENPER
033900             IF KPER-PAY-MONTHLY                                  PPBENPER
034000                 MOVE PARM-VALUE-236 TO KPER-PERS-COMP-GROSS      PPBENPER
034100                 MOVE PARM-VALUE-239 TO KPER-PERS-EXEMPT-AMOUNT   PPBENPER
034200             ELSE                                                 PPBENPER
034300                 NEXT SENTENCE.                                   PPBENPER
034400     SKIP3                                                        PPBENPER
034500 D069-SET-UP-CMP-EXIT.                                            PPBENPER
034600     EXIT.                                                        PPBENPER
034700     EJECT                                                        PPBENPER
034800 E000-CALC-PORTION SECTION.                                       PPBENPER
034900* ****************************************************************PPBENPER
035000*    THIS PARAGRAPH CALCULATES THE PORTION RETIREMENT DEDUCTION   PPBENPER
035100*     AND ACCUMULATES TO TOTAL PERS RETIREMENT DEDUCTION          PPBENPER
035200* ****************************************************************PPBENPER
035300     SKIP2                                                        PPBENPER
035400 E010-CALC-PORTION-DEDUCTION.                                     PPBENPER
035500     SKIP2                                                        PPBENPER
035600* ****************************************************************PPBENPER
035700*    THE FIRST TIME THRU THIS PARAGRAPH USE THE CALCULATED        PPBENPER
035800*     CUR MONTH RETIRE GROSS ADJUSTMENT AMOUNT IN THE CALC        PPBENPER
035900* ****************************************************************PPBENPER
036000     SKIP1                                                        PPBENPER
036100     IF DED-SUBSCRIPT < 2                                         PPBENPER
036200         COMPUTE KPER-MO-PORT-DED (DED-SUBSCRIPT) ROUNDED =       PPBENPER
036300                (KPER-CUR-MO-RETIRE-GROSS-ADJ *                   PPBENPER
036400                 KPER-CUR-MO-RETIRE-RATE) / 100                   PPBENPER
036500      ELSE                                                        PPBENPER
036600         COMPUTE KPER-MO-PORT-DED (DED-SUBSCRIPT) ROUNDED =       PPBENPER
036700                (KPER-MO-RETIRE-GROSS (DED-SUBSCRIPT) *           PPBENPER
036800                 KPER-CUR-MO-RETIRE-RATE) / 100.                  PPBENPER
036900     ADD KPER-MO-PORT-DED (DED-SUBSCRIPT),                        PPBENPER
037000         KPER-TOT-PERS-RETIRE-DEDS GIVING                         PPBENPER
037100         KPER-TOT-PERS-RETIRE-DEDS.                               PPBENPER
037200     SKIP3                                                        PPBENPER
037300 E019-CALC-PORTION-DED-EXIT.                                      PPBENPER
037400     EXIT.                                                        PPBENPER
037500     EJECT                                                        PPBENPER
037600 R000-CALL SECTION.                                               PPBENPER
037700* ****************************************************************PPBENPER
037800*    THIS PARAGRAPH ACCESSES THE SYSTEM PARAMETERS TABLE ONLY     PPBENPER
037900*     ONCE AND OBTAINS ALL THE PARAMETER VALUES NEEDED            PPBENPER
038000* ****************************************************************PPBENPER
038100     SKIP2                                                        PPBENPER
038200 R010-CALL-PPPRMUTL.                                              PPBENPER
038300     SKIP1                                                        PPBENPER
038400     CALL 'PPPRMUTL' USING SYSTEM-PARAMETER-INTERFACE.            PPBENPER
038500     SKIP3                                                        PPBENPER
038600 R019-CALL-PPPRMUTL-EXIT.                                         PPBENPER
038700     EXIT.                                                        PPBENPER
