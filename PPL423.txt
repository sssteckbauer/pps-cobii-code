000100*==========================================================%      DS413
000200*=    PROGRAM: PPL423                                     =%      DS413
000300*=    CHANGE # DS413        PROJ. REQUEST: DS413          =%      DS413
000400*=    NAME: R. P. YINGLING  MODIFICATION DATE: 07/18/89   =%      DS413
000500*=                                                        =%      DS413
000600*=    DESCRIPTION: ADDED CHANGES FOR CONVERSION FROM      =%      DS413
000700*=     OS/VS COBOL TO VS COBOL II.                        =%      DS413
000800*=                                                        =%      DS413
000900*==========================================================%      DS413
001000 IDENTIFICATION DIVISION.                                         PPL423
001100 PROGRAM-ID.       PPL423.                                        PPL423
001200 AUTHOR.           JIM PIERCE.                                    PPL423
001300 DATE-WRITTEN.     NOVEMBER 30, 1983.                             PPL423
001400                                                                  PPL423
001500                                                                  PPL423
001600 ENVIRONMENT DIVISION.                                            PPL423
001700 CONFIGURATION SECTION.                                           PPL423
001800 SPECIAL-NAMES.                                                   PPL423
001900     C01 IS TOP-PAGE.                                             PPL423
002000 SOURCE-COMPUTER.  IBM-4331.                                      PPL423
002100 OBJECT-COMPUTER.  IBM-4331.                                      PPL423
002200                                                                  PPL423
002300 INPUT-OUTPUT SECTION.                                            PPL423
002400 FILE-CONTROL.                                                    PPL423
002500                                                                  PPL423
002600     SELECT CARD-FILE  ASSIGN TO UT-S-CARDFIL.                    PPL423
002700                                                                  PPL423
002800*    SELECT PAYROLL-CONTROL-FILE COPY CPSLXPCF.                   PPL423
002900                                                                  PPL423
003000     SELECT IN-PARFILE           ASSIGN TO UT-S-PAYAUDIT.         PPL423
003100                                                                  PPL423
003200     SELECT CAMPUSFILE           ASSIGN TO UT-S-CAMPUS.           PPL423
003300                                                                  PPL423
003400     COPY CLSLCRPT.                                               PPL423
003500                                                                  PPL423
003600 DATA DIVISION.                                                   PPL423
003700 FILE SECTION.                                                    PPL423
003800                                                                  PPL423
003900 FD  CARD-FILE                                                    PPL423
004000     RECORD CONTAINS 80 CHARACTERS                                PPL423
004100     BLOCK CONTAINS 0 RECORDS                                     PPL423
004200     LABEL RECORDS ARE OMITTED.                                   PPL423
004300                                                                  PPL423
004400 01  CAMPUS-CONTROL-RECORD       PIC X(80).                       PPL423
004500                                                                  PPL423
004600*FD  PAYROLL-CONTROL-FILE                                         PPL423
004700*                                COPY CPFDXPCF.                   PPL423
004800*                                                                 PPL423
004900*01  PAYROLL-CONTROL-REC.                                         PPL423
005000*    03  PCF-KEY                 PIC X.                           PPL423
005100*    03  PCF-REC                 PIC X(224).                      PPL423
005200                                                                  PPL423
005300 FD  IN-PARFILE                                                   PPL423
005400     BLOCK CONTAINS 0 RECORDS                                     PPL423
005500     RECORDING MODE IS V                                          PPL423
005600     RECORD CONTAINS 316 TO 10681 CHARACTERS                      PPL423
005700     LABEL RECORDS ARE STANDARD.                                  PPL423
005800                                                                  PPL423
005900 01  IN-REC.                     COPY CPWSXPAR.                   DS413
006000     EJECT                                                        PPL423
006100 FD  CAMPUSFILE                                                   PPL423
006200     LABEL RECORDS ARE STANDARD                                   PPL423
006300     BLOCK CONTAINS 0 RECORDS                                     PPL423
006400     RECORD CONTAINS 94 CHARACTERS.                               PPL423
006500                                                                  PPL423
006600 01  CAMPUSREC      PIC X(94).                                    PPL423
006700                                                                  PPL423
006800     COPY CLFDCRPT.                                               PPL423
006900     EJECT                                                        PPL423
007000 WORKING-STORAGE SECTION.                                         PPL423
007100 77  SET-FLAG                    PIC 9      COMP  VALUE 1.        PPL423
007200 77  RESET-FLAG                  PIC 9      COMP  VALUE 0.        PPL423
007300*77  VPAR-DIST-MAX               PIC 9      COMP  VALUE 4.        PPL423
007400 77  VPAR-DIST-MAX               PIC 9            VALUE 3.        PPL423
007500 77  VPAR-DED-MAX                PIC 99     COMP  VALUE 20.       PPL423
007600                                                                  PPL423
007700 77  PAR-EOF-FLAG                PIC 9      COMP  VALUE 0.        PPL423
007800     88  PAR-EOF                                  VALUE 1.        PPL423
007900                                                                  PPL423
008000 77  NET-GRAND-TOTAL             PIC S9(8)V99     VALUE +0.       PPL423
008100 77  READ-COUNT                  PIC 9(6)         VALUE 0.        PPL423
008200 77  WRITE-COUNT                 PIC 9(6)         VALUE 0.        PPL423
008300 77  REJ-COUNT                   PIC 9(6)         VALUE 0.        PPL423
008400                                                                  PPL423
008500 01  DATE-FORMAT.                COPY CPWSDATE.                   DS413
008600                                                                  PPL423
008700 01  CAMPUS-CONTROL-WS.                                           PPL423
008800     03  CAMPUS-CONTROL          PIC X(6).                        PPL423
008900         88  CAMPUS              VALUE 'CAMPUS'.                  PPL423
009000         88  SURPAY              VALUE 'SURPAY'.                  PPL423
009100     03  FILLER                  PIC X(74).                       PPL423
009200                                                                  PPL423
009300 01  CAMPUS-RECORD.                                               PPL423
009400     03  CAMPUS-REC-TYPE                PIC X(1)  VALUE '6'.      PPL423
009500     03  CAMPUS-TRANS-CODE              PIC 9(2)  VALUE 0.        PPL423
009600     03  CAMPUS-TRANSIT-AND-CK.                                   PPL423
009700         05  CAMPUS-TRANSIT             PIC X(8)  VALUE SPACES.   PPL423
009800         05  CAMPUS-TRANSIT-CK          PIC X     VALUE SPACES.   PPL423
009900     03  CAMPUS-ACCOUNT                 PIC X(17) VALUE SPACES.   PPL423
010000     03  CAMPUS-AMOUNT                  PIC S9(8)V9(2).           PPL423
010100     03  CAMPUS-ID                      PIC X(15).                PPL423
010200     03  CAMPUS-NAME                    PIC X(22).                PPL423
010300     03  FILLER                         PIC X(2)  VALUE SPACES.   PPL423
010400     03  FILLER                         PIC X     VALUE '0'.      PPL423
010500     03  CAMPUS-TRACE.                                            PPL423
010600         05  CAMPUS-ODF1                PIC 9(8)  VALUE 95600614. PPL423
010700         05  CAMPUS-SEQ                 PIC 9(7)  VALUE 0.        PPL423
010800                                                                  PPL423
010900                                                                  PPL423
011000 01  WS-LITS.                                                     PPL423
011100     03  WS-LIT-NF-CNTL                 PIC X(38)   VALUE         PPL423
011200         'NO CAMPUS CONTROL RECORD FOUND        '.                PPL423
011300     03  WS-LIT-NFVAL-CNTL              PIC X(38)   VALUE         PPL423
011400         'NO VALID CAMPUS CONTROL RECORD FOUND  '.                PPL423
011500     EJECT                                                        PPL423
011600*01  XPCR-RECORD                 COPY CPWSXPCR.                   DS413
011700 01  XPCR-RECORD.                COPY CPWSXPCR.                   DS413
011800     COPY CLWSCRPT.                                               PPL423
011900     EJECT                                                        PPL423
012000 PROCEDURE DIVISION.                                              PPL423
012100                                                                  PPL423
012200 MAIN-CONTROL.                                                    PPL423
012300                                                                  PPL423
012400*    PERFORM GET-PAYROLL-CONTROL.                                 PPL423
012500     COPY CLPDCRPT.                                               PPL423
012600     OPEN  INPUT IN-PARFILE                                       PPL423
012700                 CARD-FILE.                                       PPL423
012800     OPEN OUTPUT CAMPUSFILE.                                      PPL423
012900     PERFORM GET-CAMPUS-CONTROL.                                  PPL423
013000     PERFORM PROCESS-PARFILE THRU PROCESS-PARFILE-EXIT            PPL423
013100         UNTIL PAR-EOF.                                           PPL423
013200*    DISPLAY SPACES.                                              PPL423
013300*    DISPLAY 'PAR RECORDS READ         ', READ-COUNT.             PPL423
013400*    DISPLAY 'PAR RECORDS REJECTED     ', REJ-COUNT.              PPL423
013500*    DISPLAY 'CAMPUS RECORDS WRITTEN   ', WRITE-COUNT.            PPL423
013600*    DISPLAY 'CAMPUSFILE TOTAL NET PAY ', NET-GRAND-TOTAL.        PPL423
013700*    DISPLAY SPACES.                                              PPL423
013800     PERFORM PROCESS-END THRU PROCESS-END-EXIT.                   PPL423
013900                                                                  PPL423
014000*GET-PAYROLL-CONTROL.                                             PPL423
014100                                                                  PPL423
014200****************************************************************  PPL423
014300****  PAYROLL CONTROL FILE IS EXAMINED FORM SUCESSFUL COMPLETION  PPL423
014400****  OF PREVIOUS JOB.  END DATE AND CHECK DATE ARE EXTRACTED     PPL423
014500****  FOR REPORT HEADINGS.                                        PPL423
014600****************************************************************  PPL423
014700*                                                                 PPL423
014800*    OPEN INPUT PAYROLL-CONTROL-FILE.                             PPL423
014900*    MOVE 1 TO PCF-KEY.                                           PPL423
015000*    READ PAYROLL-CONTROL-FILE                                    PPL423
015100*       AT END                                                    PPL423
015200*          DISPLAY "DISTRIBUTION PAYROLL CONTROL FILE NOT FOUND"  PPL423
015300*          DISPLAY "----  JOB ABORTED  ----"                      PPL423
015400*          STOP RUN.                                              PPL423
015500*    MOVE PCF-REC TO XPCR-RECORD.                                 PPL423
015600*    IF XPCR-I-D NOT = "PCR" OR XPCR-LAST-PGM NOT = "42"          PPL423
015700*       DISPLAY "DISTRIBUTION -- PREVIOUS PROGRAM NOT COMPLETED"  PPL423
015800*       DISPLAY "----  JOB ABORTED  ----"                         PPL423
015900*       STOP RUN.                                                 PPL423
016000*    MOVE XPCR-END-YEAR    TO HEAD1-END-YR.                       PPL423
016100*    MOVE XPCR-END-MONTH   TO HEAD1-END-MO.                       PPL423
016200*    MOVE XPCR-END-DAY     TO HEAD1-END-DA.                       PPL423
016300*    MOVE XPCR-CHECK-YEAR  TO HEAD2-CHECK-YR.                     PPL423
016400*    MOVE XPCR-CHECK-MONTH TO HEAD2-CHECK-MO.                     PPL423
016500*    MOVE XPCR-CHECK-DAY   TO HEAD2-CHECK-DA.                     PPL423
016600*    IF XPCR-CYCLE-TYPE (1) = "MA"                                PPL423
016700*        MOVE "MASM" TO HEAD1-CYCLE-TYPE                          PPL423
016800*    ELSE                                                         PPL423
016900*       MOVE XPCR-CYCLE-TYPE (1) TO HEAD1-CYCLE-TYPE.             PPL423
017000*    CLOSE PAYROLL-CONTROL-FILE.                                  PPL423
017100                                                                  PPL423
017200                                                                  PPL423
017300 GET-CAMPUS-CONTROL.                                              PPL423
017400                                                                  PPL423
017500     READ CARD-FILE INTO CAMPUS-CONTROL-WS                        PPL423
017600        AT END                                                    PPL423
017700           MOVE WS-LIT-NF-CNTL TO CR-DL7-CTL-STAT                 PPL423
017800           PERFORM PROCESS-END THRU PROCESS-END-EXIT.             PPL423
017900                                                                  PPL423
018000     IF NOT (SURPAY OR CAMPUS)                                    PPL423
018100        MOVE WS-LIT-NFVAL-CNTL TO CR-DL7-CTL-STAT                 PPL423
018200        PERFORM PROCESS-END THRU PROCESS-END-EXIT.                PPL423
018300                                                                  PPL423
018400     EJECT                                                        PPL423
018500 PROCESS-PARFILE.                                                 PPL423
018600                                                                  PPL423
018700****************************************************************  PPL423
018800***  A CAMPUS RECORD IS WRITTEN FOR EACH TYPE1 PAR RECORD WITH    PPL423
018900***  1) A NON-ZERO ID NUMBER,                                     PPL423
019000***  2) NET PAY > ZERO, AND                                       PPL423
019100***  3) NON-SUREPAY PAY DISPOSITION.                              PPL423
019200****************************************************************  PPL423
019300                                                                  PPL423
019400     READ IN-PARFILE                                              PPL423
019500        AT END                                                    PPL423
019600           MOVE 1 TO PAR-EOF-FLAG                                 PPL423
019700           GO TO PROCESS-PARFILE-EXIT.                            PPL423
019800     ADD 1 TO READ-COUNT.                                         PPL423
019900     IF XPAR-ID-NO = ZEROS                                        PPL423
020000        GO TO PROCESS-PARFILE.                                    PPL423
020100*    IF SPL-TYPE NOT = 1                                          PPL423
020200*       ADD 1 TO REJ-COUNT                                        PPL423
020300*       GO TO PROCESS-PARFILE.                                    PPL423
020400*    MOVE VPAR-KEY1 TO XPAR-KEY.                                  PPL423
020500*    MOVE VPAR-FIX1 TO XPAR-FIXED-PORTION.                        PPL423
020600     IF XPAR-ID-NO = 0 OR XPAR-NET-PAY = 0                        PPL423
020700        ADD 1 TO REJ-COUNT                                        PPL423
020800        GO TO PROCESS-PARFILE.                                    PPL423
020900     IF CAMPUS AND XPAR-PAY-DISPOSITION = '1'                     PPL423
021000        ADD 1 TO REJ-COUNT                                        PPL423
021100        GO TO PROCESS-PARFILE.                                    PPL423
021200     MOVE XPAR-NET-PAY TO CAMPUS-AMOUNT.                          PPL423
021300     MOVE XPAR-ID-NO   TO CAMPUS-ID.                              PPL423
021400     MOVE XPAR-NAME-26 TO CAMPUS-NAME.                            PPL423
021500     ADD  XPAR-NET-PAY TO NET-GRAND-TOTAL.                        PPL423
021600     WRITE CAMPUSREC FROM CAMPUS-RECORD.                          PPL423
021700     ADD 1 TO WRITE-COUNT.                                        PPL423
021800     GO TO PROCESS-PARFILE.                                       PPL423
021900                                                                  PPL423
022000                                                                  PPL423
022100 PROCESS-PARFILE-EXIT.                                            PPL423
022200     EXIT.                                                        PPL423
022300                                                                  PPL423
022400 PROCESS-END.                                                     PPL423
022500                                                                  PPL423
022600     PERFORM PRINT-CONTROL-REPORT.                                PPL423
022700     CLOSE IN-PARFILE, CARD-FILE, CAMPUSFILE.                     PPL423
022800     STOP RUN.                                                    PPL423
022900                                                                  PPL423
023000 PROCESS-END-EXIT.                                                PPL423
023100     EXIT.                                                        PPL423
023200*    COPY CLPDL423.                                               DS413
023300*                                                                 DS413
023400*                                                                 DS413
023500*      CONTROL REPORT PPL423CR                                    DS413
023600*                                                                 DS413
023700*                                                                 DS413
023800 PRINT-CONTROL-REPORT SECTION.                                    DS413
023900                                                                  DS413
024000     OPEN OUTPUT CONTROLREPORT.                                   DS413
024100                                                                  DS413
024200     MOVE 'PPL423CR'             TO CR-HL1-RPT.                   DS413
024300     MOVE CR-HDG-LINE1           TO CONTROL-DATA.                 DS413
024400     WRITE CONTROLRECORD AFTER ADVANCING PAGE.                    DS413
024500                                                                  DS413
024600     MOVE CR-HDG-LINE2           TO CONTROL-DATA.                 DS413
024700     WRITE CONTROLRECORD AFTER ADVANCING 1 LINES.                 DS413
024800                                                                  DS413
024900     MOVE WHEN-COMPILED          TO COMPILE-TIME-AND-DATE.        DS413
025000     MOVE COMPILE-TIME           TO CR-HL3-CMP-TIME.              DS413
025100     MOVE COMPILE-DATE           TO CR-HL3-CMP-DATE.              DS413
025200     MOVE CR-HDG-LINE3           TO CONTROL-DATA.                 DS413
025300     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 DS413
025400                                                                  DS413
025500     MOVE CR-HDG-LINE4           TO CONTROL-DATA.                 DS413
025600     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 DS413
025700                                                                  DS413
025800     MOVE NO-CONTROL-CARD        TO CR-DL5-CTL-CARD.              DS413
025900     MOVE CR-DET-LINE5           TO CONTROL-DATA.                 DS413
026000     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 DS413
026100                                                                  DS413
026200     MOVE CR-HDG-LINE6           TO CONTROL-DATA.                 DS413
026300     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 DS413
026400                                                                  DS413
026410*    ACCEPT TIME-WORK-AREA FROM TIME.                             DS413
026500     MOVE TIME-OF-DAY            TO TIME-WORK-AREA.               DS413
026600     MOVE CORR TIME-WORK-AREA    TO TIME-WORK-AREA-SPLIT.         DS413
026700     MOVE TIME-WORK-AREA-SPLIT   TO CR-DL7-CTL-TIME.              DS413
026800     MOVE CR-DET-LINE7           TO CONTROL-DATA.                 DS413
026900     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 DS413
027000                                                                  DS413
027100     MOVE CR-HDG-LINE8           TO CONTROL-DATA.                 DS413
027200     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 DS413
027300*                                                                 DS413
027400*  FOLLOWING CODE MUST BE DUPLICATED FOR EVERY FILE               DS413
027500*                                                                 DS413
027600*  FILE NAME, ACTION (SO-CREATED OR SO-READ), AND                 DS413
027700*  THE ACTUAL RECORD COUNTER MUST BE FILLED IN                    DS413
027800*                                                                 DS413
027900     MOVE 'PAYAUDIT'             TO CR-DL9-FILE.                  DS413
028000     MOVE SO-READ                TO CR-DL9-ACTION.                DS413
028100     MOVE READ-COUNT             TO CR-DL9-VALUE.                 DS413
028200     MOVE CR-DET-LINE9           TO CONTROL-DATA.                 DS413
028300     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 DS413
028400*                                                                 DS413
028500     MOVE 'CAMPUS'               TO CR-DL9-FILE.                  DS413
028600     MOVE SO-CREATED             TO CR-DL9-ACTION.                DS413
028700     MOVE WRITE-COUNT            TO CR-DL9-VALUE.                 DS413
028800     MOVE CR-DET-LINE9           TO CONTROL-DATA.                 DS413
028900     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 DS413
029000*                                                                 DS413
029100*                                                                 DS413
029200*                                                                 DS413
029300     MOVE END-OF-REPORT-MSG      TO CONTROL-DATA.                 DS413
029400     WRITE CONTROLRECORD AFTER ADVANCING 4 LINES.                 DS413
029500                                                                  DS413
029600     CLOSE CONTROLREPORT.                                         DS413
