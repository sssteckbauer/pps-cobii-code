000100**************************************************************/   30930413
000200*  PROGRAM: PPBENLTD                                         */   30930413
000300*  RELEASE: ____0413____  SERVICE REQUEST(S): ____3093____   */   30930413
000400*  NAME:____JIM WILLIAMS__CREATION DATE:      __06/26/89__   */   30930413
000500*  DESCRIPTION:                                              */   30930413
000600*  - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II. */   30930413
000601**************************************************************/   30930413
000602**************************************************************/   45360412
000603*  PROGRAM: PPBENLTD                                         */   45360412
000604*  RELEASE: ____0412____  SERVICE REQUEST(S): ____4536____   */   45360412
000605*  NAME:    __J.WILCOX__  MODIFICATION DATE:  __06/07/89__   */   45360412
000606*  DESCRIPTION:                                              */   45360412
000607*                                                            */   45360412
000608*    - ELIMINATE USE OF LTD PLAN CODE.  THERE IS NOW A       */   45360412
000609*      SINGLE LTD PLAN.  THERE WILL NOW BE ONLY A SINGLE     */   45360412
000610*      SALARY MAXIMUM AND A SINGLE SET OF LTD RATES TO BE    */   45360412
000620*      USED BY THE PROGRAM.                                  */   45360412
000630**************************************************************/   45360412
000640**************************************************************/   14870327
000650*  PROGRAM: PPBENLTD                                         */   14870327
000660*  REL:  -----0327---------  SERVICE REQEUSTS: __1487________*/   14870327
000670*                                                            */   14870327
000680*  NAME _S._ISAACS_____   MODIFICATION DATE ____11/10/87_____*/   14870327
000690*  DESCRIPTION                                               */   14870327
000700*                                                            */   14870327
000800*  S.R. 1487: NEW AGE GROUP FOR STD, LDT AND GLI             */   14870327
000900*                                                            */   14870327
001000**************************************************************/   14870327
001100**************************************************************/   14240278
001200*  PROGRAM:  PPBENLTD                                        */   14240278
001300*  REL#:  0278   REF: NONE  SERVICE REQUESTS:   ____1424____ */   14240278
001400*  NAME ___BMB_________   MODIFICATION DATE ____01/20/87_____*/   14240278
001500*  DESCRIPTION                                               */   14240278
001600*      THIS CODE WAS REMOVED FROM USER40 AND PLACED IN       */   14240278
001700*   THIS STAND ALONE CALLED MODULE.                          */   14240278
001800*                                                            */   14240278
001900**************************************************************/   14240278
002000 IDENTIFICATION DIVISION.                                         PPBENLTD
002100 PROGRAM-ID. PPBENLTD.                                            PPBENLTD
002200*SOURCE-COMPUTER.                          COPY 'CPOTXUCS'.       30930413
002300*OBJECT-COMPUTER.                          COPY 'CPOTXOBJ'.       30930413
002400 INSTALLATION. UNIVERSITY OF CALIFORNIA.                          PPBENLTD
002500 AUTHOR.                                                          PPBENLTD
002600*        BRUCE M. BRISCOE, CREATIVELY APPLIED TECHNOLOGIES, INC.  14870327
002700*                                                              CD.14870327
002800 DATE-WRITTEN.                                                    PPBENLTD
002900 DATE-COMPILED.                                                   PPBENLTD
003000*REMARKS.                                                         30930413
003100******************************************************************PPBENADD
003200*                                                                *PPBENADD
003300*  THIS MODULE PERFORMS LONG TERM DISABILITY RATE AND PREMIUM    *PPBENADD
003400* CALCULATIONS.                                                  *PPBENADD
003500*                                                                *PPBENADD
003600*  RECOMMENDED PRACTICE: CALLING PROGRAM SHOULD CALL THIS        *PPBENADD
003700* MODULE TO 'INITIALIZE' DURING CALLING PROGRAM INITIALIZATION.  *PPBENADD
003800*                                                                *PPBENADD
003900*                                                                *PPBENADD
004000******************************************************************PPBENADD
004100 ENVIRONMENT DIVISION.                                            PPBENLTD
004200 CONFIGURATION SECTION.                                           PPBENLTD
004210 SOURCE-COMPUTER.                          COPY 'CPOTXUCS'.       30930413
004220 OBJECT-COMPUTER.                          COPY 'CPOTXOBJ'.       30930413
004300 SPECIAL-NAMES.                                                   PPBENLTD
004400 INPUT-OUTPUT SECTION.                                            PPBENLTD
004500 FILE-CONTROL.                                                    PPBENLTD
004600     SKIP3                                                        PPBENLTD
004700*                                                              CD.14870327
004800 DATA DIVISION.                                                   PPBENLTD
004900 FILE SECTION.                                                    PPBENLTD
005000     SKIP1                                                        PPBENLTD
005100     EJECT                                                        PPBENLTD
005200***                                                            CD.14870327
005300 WORKING-STORAGE SECTION.                                         PPBENLTD
005400     SKIP1                                                        PPBENLTD
005500 01  WS-ID                        PIC X(37)                       PPBENLTD
005600     VALUE 'PPBENLTD WORKING-STORAGE BEGINS HERE'.                PPBENLTD
005700     SKIP3                                                        PPBENLTD
005800 01  AGE-SUBSCRIPT                   PIC 9(02).                   PPBENLTD
005900 01  RATE-SUBSCRIPT                  PIC 9(02).                   PPBENLTD
006000*01  PLAN-SUBSCRIPT                  PIC 9(02).                   45360412
006100     SKIP3                                                        PPBENLTD
006200*    *******************************************                  PPBENLTD
006300*    * USED TO GET PREMIUM AMOUNT BY PLAN CODE                    PPBENLTD
006400*    *******************************************                  PPBENLTD
006500 01  FLAGS-AND-SWITCHES.                                          PPBENLTD
006600     05  PROGRAM-STATUS-FLAG         PIC X(01)   VALUE '0'.       PPBENLTD
006700         88  PROGRAM-STATUS-NORMAL               VALUE '0'.       PPBENLTD
006800         88  PROGRAM-STATUS-EXITING              VALUE '1'.       PPBENLTD
006900     05  FOUND-RATE-SW               PIC X(01)   VALUE 'N'.       PPBENLTD
007000         88  FOUND-RATE                          VALUE 'Y'.       PPBENLTD
007100     SKIP1                                                        PPBENLTD
007200 01  HOLD-VALUES-AREA.                                            PPBENLTD
007300*    *************************************************************PPBENLTD
007400*    * THE FOLLOWING IS USED TO HOLD DATA                        *PPBENLTD
007500*    *************************************************************PPBENLTD
007600     05  WS-CALC-SALARY-BASE         PIC 9(5)V99.                 PPBENLTD
007700******************************************************************PPBENLTD
007800*                USED FOR DATE CALCS                             *PPBENLTD
007900******************************************************************PPBENLTD
008000     05  WS-PPEND-DATE-HOLD       PIC 9(6).                       PPBENLTD
008100     05  WS-PPEND-AREA  REDEFINES                                 PPBENLTD
008200         WS-PPEND-DATE-HOLD.                                      PPBENLTD
008300         10  WS-PPEND-YYMM        PIC 9(4).                       PPBENLTD
008400         10  FILLER               PIC XX.                         PPBENLTD
008500*                                                                *PPBENLTD
008600     05  WS-EFFECT-DATE-HOLD      PIC 9(6).                       PPBENLTD
008700     05  WS-EFFECT-AREA REDEFINES                                 PPBENLTD
008800         WS-EFFECT-DATE-HOLD.                                     PPBENLTD
008900         10  WS-EFFECT-YYMM       PIC 9(4).                       PPBENLTD
009000         10  FILLER               PIC XX.                         PPBENLTD
009100*                                                                *PPBENLTD
009200     SKIP1                                                        PPBENLTD
009300 01  CONSTANT-VALUES.                                             PPBENLTD
009400*    *************************************************************PPBENLTD
009500*    * THE FOLLOWING SHOULD BE THE SAME VALUE AS FOR THE OCCURS ABPPBENLTD
009600*    *************************************************************PPBENLTD
009700     05  CONSTANT-BENEFIT-TYPE       PIC X(01)   VALUE 'L'.       PPBENLTD
009800     05  CONSTANT-NORMAL             PIC X(01)   VALUE '0'.       PPBENLTD
009900     05  CONSTANT-ABORT              PIC X(01)   VALUE '1'.       PPBENLTD
010000     05  CONSTANT-EXIT-PROGRAM       PIC X(01)   VALUE '1'.       PPBENLTD
010100     EJECT                                                        PPBENLTD
010110 01  XUTF-UTILITY-FUNCTIONS.                                      30930413
010200                                  COPY 'CPWSXUTF'.                PPBENLTD
010300     EJECT                                                        PPBENLTD
010310 01  XBRT-BENEFITS-RATES-RECORD.                                  30930413
010400                                  COPY 'CPWSXBRT'.                PPBENLTD
010500     SKIP3                                                        PPBENLTD
010600 01  FILLER                       PIC X(37)                       PPBENLTD
010700     VALUE 'PPBENLTD WORKING-STORAGE ENDS HERE'.                  PPBENLTD
010800     EJECT                                                        PPBENLTD
010900*                                                              CD.14870327
011000 LINKAGE SECTION.                                                 PPBENLTD
011100     SKIP1                                                        PPBENLTD
011200 01  PPBENLTD-INTERFACE.          COPY 'CPLNKLTD'.                PPBENLTD
011300     EJECT                                                        PPBENLTD
011310 01  PPBUTUTL-INTERFACE.                                          30930413
011400                                  COPY 'CPWSBUTI'.                PPBENLTD
011500     EJECT                                                        PPBENLTD
011510 01  CTL-INTERFACE.                                               30930413
011600                                  COPY 'CPWSXCIF'.                PPBENLTD
011700     SKIP3                                                        PPBENLTD
011710 01  CTL-SEGMENT-TABLE.                                           30930413
011800                                  COPY 'CPWSXCST'.                PPBENLTD
011900     EJECT                                                        PPBENLTD
012000***                                                            CD.14870327
012100     SKIP1                                                        PPBENLTD
012200 PROCEDURE DIVISION USING PPBENLTD-INTERFACE                      PPBENLTD
012300                          PPBUTUTL-INTERFACE                      PPBENLTD
012400                          CTL-INTERFACE                           PPBENLTD
012500                          CTL-SEGMENT-TABLE.                      PPBENLTD
012600     SKIP1                                                        PPBENLTD
012700 A000-MAINLINE SECTION.                                           PPBENLTD
012800     SKIP2                                                        PPBENLTD
012900     MOVE CONSTANT-NORMAL TO PROGRAM-STATUS-FLAG.                 PPBENLTD
013000     SKIP1                                                        PPBENLTD
013100     PERFORM B010-INITIALIZE                                      PPBENLTD
013200        THRU B019-INITIALIZE-EXIT.                                PPBENLTD
013300     SKIP1                                                        PPBENLTD
013400     IF PROGRAM-STATUS-NORMAL                                     PPBENLTD
013500     THEN                                                         PPBENLTD
013600        IF KLTD-ACTION-RATE-RETRIEVAL OR                          PPBENLTD
013700           KLTD-ACTION-PREMIUM-CALC                               PPBENLTD
013800        THEN                                                      PPBENLTD
013900            PERFORM C010-GET-RATE                                 PPBENLTD
014000               THRU C019-GET-RATE-EXIT                            PPBENLTD
014100            IF PROGRAM-STATUS-NORMAL    AND                       PPBENLTD
014200               KLTD-ACTION-PREMIUM-CALC AND                       PPBENLTD
014300               FOUND-RATE                                         PPBENLTD
014400            THEN                                                  PPBENLTD
014500                PERFORM D010-CALCULATE                            PPBENLTD
014600                   THRU D019-CALCULATE-EXIT                       PPBENLTD
014700            ELSE                                                  PPBENLTD
014800                NEXT SENTENCE.                                    PPBENLTD
014900     SKIP3                                                        PPBENLTD
015000 A009-GOBACK.                                                     PPBENLTD
015100     GOBACK.                                                      PPBENLTD
015200     EJECT                                                        PPBENLTD
015300 B000-INITIALIZATION SECTION.                                     PPBENLTD
015400     SKIP2                                                        PPBENLTD
015500 B010-INITIALIZE.                                                 PPBENLTD
015600     SKIP1                                                        PPBENLTD
015700     MOVE CONSTANT-NORMAL TO KLTD-RETURN-STATUS-FLAG.             PPBENLTD
015800*                                                                 PPBENLTD
015900******************************************************************PPBENLTD
016000*      SET NUMERIC RETURN FIELDS TO ZERO                         *PPBENLTD
016100******************************************************************PPBENLTD
016200*                                                                 PPBENLTD
016300     MOVE ZERO TO KLTD-RATE-AMOUNT                                PPBENLTD
016400                  KLTD-PREMIUM-AMOUNT                             PPBENLTD
016500                  KLTD-MAX-SAL-BASE                               PPBENLTD
016600                  KLTD-DED-AMOUNT.                                PPBENLTD
016700     SKIP1                                                        PPBENLTD
016800     IF KLTD-ACTION-RATE-RETRIEVAL OR                             PPBENLTD
016900        KLTD-ACTION-PREMIUM-CALC                                  PPBENLTD
017000         PERFORM B030-VALIDATE-LOOKUP-ARGS                        PPBENLTD
017100            THRU B039-VALIDATE-LOOKUP-ARGS-EXIT                   PPBENLTD
017200     ELSE                                                         PPBENLTD
017300         NEXT SENTENCE.                                           PPBENLTD
017400     SKIP1                                                        PPBENLTD
017500     IF KLTD-INVALID-LOOKUP-ARG                                   PPBENLTD
017600         NEXT SENTENCE                                            PPBENLTD
017700     ELSE                                                         PPBENLTD
017800         IF KLTD-ACTION-PREMIUM-CALC                              PPBENL
017900             PERFORM B050-VALIDATE-ENROLLMENT                     PPBENL
018000                THRU B059-VALIDATE-ENROLLMENT-EXIT                PPBENL
018100         ELSE                                                     PPBENL
018200             NEXT SENTENCE.                                       PPBENL
018300     SKIP3                                                        PPBENLTD
018400 B019-INITIALIZE-EXIT.                                            PPBENLTD
018500     EXIT.                                                        PPBENLTD
018600     SKIP3                                                        PPBENLTD
018700 B030-VALIDATE-LOOKUP-ARGS.                                       PPBENLTD
018800     SKIP1                                                        PPBENLTD
018900     MOVE '0' TO KLTD-INVALID-LOOKUP-ARG-FLAG.                    PPBENLTD
019000     SKIP1                                                        PPBENLTD
019100     IF  KLTD-INVALID-AGE                                         PPBENLTD
019200         MOVE '1' TO KLTD-INVALID-LOOKUP-ARG-FLAG                 PPBENLTD
019300     ELSE                                                         PPBENLTD
019400         NEXT SENTENCE.                                           PPBENLTD
019500     SKIP1                                                        PPBENLTD
019600*****IF  KLTD-VALID-PLAN-CODE                                     45360412
019700*****    NEXT SENTENCE                                            45360412
019800*****ELSE                                                         45360412
019900*****    MOVE '1' TO KLTD-INVALID-LOOKUP-ARG-FLAG.                45360412
020000*****SKIP1                                                        45360412
020100     IF KLTD-INVALID-LOOKUP-ARG                                   PPBENLTD
020200         MOVE '1' TO PROGRAM-STATUS-FLAG.                         PPBENLTD
020300     SKIP1                                                        PPBENLTD
020400     MOVE '1' TO KLTD-NOT-ENROLLED-IN-LTD-FLAG.                   PPBENLTD
020500     SKIP3                                                        PPBENLTD
020600 B039-VALIDATE-LOOKUP-ARGS-EXIT.                                  PPBENLTD
020700     EXIT.                                                        PPBENLTD
020800     SKIP3                                                        PPBENLTD
020900 B050-VALIDATE-ENROLLMENT.                                        PPBENLTD
021000     SKIP1                                                        PPBENLTD
021100     MOVE '0' TO KLTD-NOT-ENROLLED-IN-LTD-FLAG.                   PPBENLTD
021200     MOVE KLTD-EARNINGS-EFFECT-DATE  TO WS-EFFECT-DATE-HOLD.      PPBENLTD
021300     MOVE KLTD-PERIOD-END-DATE       TO WS-PPEND-DATE-HOLD.       PPBENLTD
021400     IF WS-EFFECT-YYMM < WS-PPEND-YYMM                            PPBENLTD
021500                OR                                                PPBENLTD
021600        WS-EFFECT-YYMM = WS-PPEND-YYMM                            PPBENLTD
021700         NEXT SENTENCE                                            PPBENLTD
021800     ELSE                                                         PPBENLTD
021900         MOVE '1' TO KLTD-NOT-ENROLLED-IN-LTD-FLAG.               PPBENLTD
022000     SKIP1                                                        PPBENLTD
022100     IF  KLTD-INVALID-SALARY-BASE                                 PPBENLTD
022200         MOVE '1' TO KLTD-INVALID-LOOKUP-ARG-FLAG                 PPBENLTD
022300     ELSE                                                         PPBENLTD
022400         NEXT SENTENCE.                                           PPBENLTD
022500     SKIP3                                                        PPBENLTD
022600     IF KLTD-NOT-ENROLLED-IN-LTD                                  PPBENLTD
022700               OR                                                 PPBENLTD
022800        KLTD-INVALID-LOOKUP-ARG                                   PPBENLTD
022900         MOVE '1' TO PROGRAM-STATUS-FLAG.                         PPBENLTD
023000     SKIP1                                                        PPBENLTD
023100 B059-VALIDATE-ENROLLMENT-EXIT.                                   PPBENLTD
023200     EXIT.                                                        PPBENLTD
023300     EJECT                                                        PPBENLTD
023400 C000-RATE-RETRIEVAL SECTION.                                     PPBENLTD
023500     SKIP2                                                        PPBENLTD
023600 C010-GET-RATE.                                                   PPBENLTD
023700     SKIP1                                                        PPBENLTD
023800     MOVE PPBUTUTL-VLDT-BENS    TO BUTI-IN-ACTION.                PPBENLTD
023900     PERFORM P030-GET-KEY-FOR-RATE                                PPBENLTD
024000        THRU P039-GET-KEY-FOR-RATE-EXIT.                          PPBENLTD
024100     SKIP1                                                        PPBENLTD
024200     MOVE 'N' TO FOUND-RATE-SW.                                   PPBENLTD
024300     SKIP1                                                        PPBENLTD
024400     PERFORM P010-BRT-LOOKUP-RATE-LOOP                            PPBENLTD
024500        THRU P019-BRT-LOOKUP-RATE-LOOP-EXIT                       PPBENLTD
024600             UNTIL FOUND-RATE OR                                  PPBENLTD
024700*                                                                 PPBENLTD
024800******************************************************************PPBENLTD
024900*                BLANK KEY INDICATES NOT ELIGIBLE FOR BENEFITS   *PPBENLTD
025000******************************************************************PPBENLTD
025100*                                                                 PPBENLTD
025200                   BUTI-OUT-BRT-KEY = SPACE OR                    PPBENLTD
025300                   NOT PROGRAM-STATUS-NORMAL.                     PPBENLTD
025400     SKIP3                                                        PPBENLTD
025500     IF FOUND-RATE                                                PPBENLTD
025600         PERFORM C030-CALC-RATE                                   PPBENLTD
025700            THRU C039-CALC-RATE-EXIT                              PPBENLTD
025800     ELSE                                                         PPBENLTD
025900         MOVE '1' TO PROGRAM-STATUS-FLAG                          PPBENLTD
026000         MOVE '1' TO KLTD-NOT-ENROLLED-IN-LTD-FLAG.               PPBENLTD
026100     SKIP3                                                        PPBENLTD
026200 C019-GET-RATE-EXIT.                                              PPBENLTD
026300     EXIT.                                                        PPBENLTD
026400     SKIP3                                                        PPBENLTD
026500 C030-CALC-RATE.                                                  PPBENLTD
026600     SKIP1                                                        PPBENLTD
026700*****MOVE KLTD-PLAN-CODE TO PLAN-SUBSCRIPT.                       45360412
026800     SKIP1                                                        PPBENLTD
026900*****MOVE XBRT-LTD-MAX-SAL-BASE(PLAN-SUBSCRIPT)                   45360412
027000*******TO KLTD-MAX-SAL-BASE.                                      45360412
027100     MOVE XBRT-LTD-MAX-SAL-BASE TO KLTD-MAX-SAL-BASE              45360412
027200     SKIP1                                                        PPBENLTD
027300     MOVE ZEROS TO RATE-SUBSCRIPT.                                PPBENLTD
027400     PERFORM C040-LOOK-UP-AGE                                     PPBENLTD
027500        THRU C049-LOOK-UP-AGE-EXIT                                PPBENLTD
027600             VARYING AGE-SUBSCRIPT FROM 1 BY 1                    PPBENLTD
027700               UNTIL AGE-SUBSCRIPT > 10.                          14870327
027800*****          UNTIL AGE-SUBSCRIPT > 9.                           14870327
027900     SKIP1                                                        PPBENLTD
028000     IF RATE-SUBSCRIPT = ZEROS                                    PPBENLTD
028100         MOVE '1' TO KLTD-INVALID-LOOKUP-ARG-FLAG                 PPBENLTD
028200         MOVE '1' TO PROGRAM-STATUS-FLAG                          PPBENLTD
028300     ELSE                                                         PPBENLTD
028400         MOVE XBRT-LTD-RATE(RATE-SUBSCRIPT) TO KLTD-RATE-AMOUNT.  45360412
028500*********MOVE XBRT-LTD-RATE(PLAN-SUBSCRIPT, RATE-SUBSCRIPT)       45360412
028600***********TO KLTD-RATE-AMOUNT.                                   45360412
028700     SKIP3                                                        PPBENLTD
028800 C039-CALC-RATE-EXIT.                                             PPBENLTD
028900     EXIT.                                                        PPBENLTD
029000     SKIP3                                                        PPBENLTD
029100 C040-LOOK-UP-AGE.                                                PPBENLTD
029200     SKIP1                                                        PPBENLTD
029300     IF KLTD-EMPLOYEE-AGE NOT LESS THAN                           PPBENLTD
029400        XBRT-LTD-MIN-AGE(AGE-SUBSCRIPT)                           PPBENLTD
029500                 AND                                              PPBENLTD
029600        KLTD-EMPLOYEE-AGE NOT GREATER THAN                        PPBENLTD
029700        XBRT-LTD-MAX-AGE(AGE-SUBSCRIPT)                           PPBENLTD
029800         MOVE AGE-SUBSCRIPT TO RATE-SUBSCRIPT                     PPBENLTD
029900     ELSE                                                         PPBENLTD
030000         NEXT SENTENCE.                                           PPBENLTD
030100     SKIP3                                                        PPBENLTD
030200 C049-LOOK-UP-AGE-EXIT.                                           PPBENLTD
030300     EXIT.                                                        PPBENLTD
030400     EJECT                                                        PPBENLTD
030500 D000-ADD-CALC SECTION.                                           PPBENLTD
030600     SKIP2                                                        PPBENLTD
030700 D010-CALCULATE.                                                  PPBENLTD
030800     SKIP1                                                        PPBENLTD
030900*****IF KLTD-SALARY-BASE > XBRT-LTD-MAX-SAL-BASE(PLAN-SUBSCRIPT)  45360412
031000*********MOVE XBRT-LTD-MAX-SAL-BASE(PLAN-SUBSCRIPT)               45360412
031100     IF KLTD-SALARY-BASE > XBRT-LTD-MAX-SAL-BASE                  45360412
031200         MOVE XBRT-LTD-MAX-SAL-BASE                               45360412
031300           TO WS-CALC-SALARY-BASE                                 PPBENLTD
031400     ELSE                                                         PPBENLTD
031500         MOVE KLTD-SALARY-BASE                                    PPBENLTD
031600           TO WS-CALC-SALARY-BASE.                                PPBENLTD
031700     SKIP1                                                        PPBENLTD
031800     COMPUTE KLTD-PREMIUM-AMOUNT ROUNDED =                        PPBENLTD
031900             WS-CALC-SALARY-BASE *                                PPBENLTD
032000             XBRT-LTD-RATE(RATE-SUBSCRIPT).                       45360412
032100*************XBRT-LTD-RATE(PLAN-SUBSCRIPT, RATE-SUBSCRIPT).       45360412
032200     SKIP1                                                        PPBENLTD
032300     MOVE KLTD-PREMIUM-AMOUNT TO KLTD-DED-AMOUNT.                 PPBENLTD
032400     SKIP3                                                        PPBENLTD
032500 D019-CALCULATE-EXIT.                                             PPBENLTD
032600     EXIT.                                                        PPBENLTD
032700     EJECT                                                        PPBENLTD
032800 P000-SET-UP-BRT-KEY SECTION.                                     PPBENLTD
032900     SKIP2                                                        PPBENLTD
033000 P010-BRT-LOOKUP-RATE-LOOP.                                       PPBENLTD
033100     SKIP1                                                        PPBENLTD
033200     MOVE 'N' TO FOUND-RATE-SW.                                   PPBENLTD
033300     SKIP1                                                        PPBENLTD
033400*                                                                 PPBENLTD
033500******************************************************************PPBENLTD
033600*      TO AVOID UNNECESSARY DISK ACCESS, CHECK PREVIOUS BEFORE   *PPBENLTD
033700*      DOING LOOKUP PHYSICALLY IN TABLE                          *PPBENLTD
033800******************************************************************PPBENLTD
033900*                                                                 PPBENLTD
034000     SKIP1                                                        PPBENLTD
034100     IF XBRT-KEY = BUTI-OUT-BRT-KEY                               PPBENLTD
034200         MOVE 'Y' TO FOUND-RATE-SW                                PPBENLTD
034300     ELSE                                                         PPBENLTD
034400*                                                                 PPBENLTD
034500*        ************************************                     PPBENLTD
034600*        * RETRIEVE RATE FROM TABLE USING KEY                     PPBENLTD
034700*        ************************************                     PPBENLTD
034800*                                                                 PPBENLTD
034900         PERFORM P050-ACCESS-CTL                                  PPBENLTD
035000            THRU P059-ACCESS-CTL-EXIT.                            PPBENLTD
035100     SKIP3                                                        PPBENLTD
035200 P019-BRT-LOOKUP-RATE-LOOP-EXIT.                                  PPBENLTD
035300     EXIT.                                                        PPBENLTD
035400     SKIP3                                                        PPBENLTD
035500 P030-GET-KEY-FOR-RATE.                                           PPBENLTD
035600     SKIP1                                                        PPBENLTD
035700*                                                                 PPBENLTD
035800******************************************************************PPBENLTD
035900*      COLLECTIVE BARGAINING INFORMATION IS PASSED TO PPBUTUTL   *PPBENLTD
036000*      BY SAME PROGRAM THAT CALLED THIS MODULE--SO THIS MODULE   *PPBENLTD
036100*      DOES NOT NEED TO BE AWARE OF DETAILS OF THOSE ARGUMENTS.  *PPBENLTD
036200******************************************************************PPBENLTD
036300*                                                                 PPBENLTD
036400     SKIP1                                                        PPBENLTD
036500     MOVE CONSTANT-BENEFIT-TYPE TO BUTI-IN-BEN-TYPE.              PPBENLTD
036600     MOVE SPACES                TO BUTI-IN-BEN-PLAN.              PPBENLTD
036700     PERFORM R010-CALL-PPBUTUTL                                   PPBENLTD
036800        THRU R019-CALL-PPBUTUTL-EXIT.                             PPBENLTD
036900     SKIP1                                                        PPBENLTD
037000     IF BUTI-OUT-ERROR-FLAG = 'Y'                                 PPBENLTD
037100         MOVE CONSTANT-ABORT TO KLTD-RETURN-STATUS-FLAG           PPBENLTD
037200         MOVE CONSTANT-EXIT-PROGRAM TO PROGRAM-STATUS-FLAG        PPBENLTD
037300     ELSE                                                         PPBENLTD
037400         NEXT SENTENCE.                                           PPBENLTD
037500     SKIP3                                                        PPBENLTD
037600 P039-GET-KEY-FOR-RATE-EXIT.                                      PPBENLTD
037700     EXIT.                                                        PPBENLTD
037800     SKIP3                                                        PPBENLTD
037900 P050-ACCESS-CTL.                                                 PPBENLTD
038000     SKIP1                                                        PPBENLTD
038100     MOVE BUTI-OUT-BRT-KEY TO IO-CTL-NOM-KEY.                     PPBENLTD
038200     PERFORM R020-CALL-PPIOCTL                                    PPBENLTD
038300        THRU R029-CALL-PPIOCTL-EXIT.                              PPBENLTD
038400     SKIP1                                                        PPBENLTD
038500     IF IO-CTL-ERROR-CODE NOT = '00' AND                          PPBENLTD
038600                          NOT = '05' AND                          PPBENLTD
038700                          NOT = '06' AND                          PPBENLTD
038800                          NOT = '13' AND                          PPBENLTD
038900                          NOT = '21'                              PPBENLTD
039000*                                                                 PPBENLTD
039100*        ********************************************             PPBENLTD
039200*        * ABORT PROGRAM DUE TO ERROR READING TABLE *             PPBENLTD
039300*        ********************************************             PPBENLTD
039400*                                                                 PPBENLTD
039500         MOVE CONSTANT-ABORT TO KLTD-RETURN-STATUS-FLAG           PPBENLTD
039600         MOVE CONSTANT-EXIT-PROGRAM TO PROGRAM-STATUS-FLAG        PPBENLTD
039700     ELSE                                                         PPBENLTD
039800         IF IO-CTL-ERROR-CODE = '00' AND                          PPBENLTD
039900            CTL-SEG-TAB-DEL NOT = HIGH-VALUE                      PPBENLTD
040000*                                                                 PPBENLTD
040100*        *****************************************************    PPBENLTD
040200*        * HIGH-VALUE WOULD INDICATE THAT RECORD IS DELETED, *    PPBENLTD
040300*        * WHICH WOULD NOT BE A TRUE "FIND"                  *    PPBENLTD
040400*        *****************************************************    PPBENLTD
040500*                                                                 PPBENLTD
040600             MOVE 'Y' TO FOUND-RATE-SW                            PPBENLTD
040700         ELSE                                                     PPBENLTD
040800             IF IO-CTL-ERROR-CODE  = '05' OR '06'                 PPBENLTD
040900                                  OR '13' OR '21'                 PPBENLTD
041000*                                                                 PPBENLTD
041100*        *********************************************************PPBENLTD
041200*        * RATE NOT FOUND, SO GET KEY FOR NEXT ITERATION OF LOOP *PPBENLTD
041300*        *********************************************************PPBENLTD
041400*                                                                 PPBENLTD
041500                MOVE PPBUTUTL-BEN-RATES-DEFAULT TO BUTI-IN-ACTION PPBENLTD
041600                PERFORM P030-GET-KEY-FOR-RATE                     PPBENLTD
041700                   THRU P039-GET-KEY-FOR-RATE-EXIT                PPBENLTD
041800             ELSE                                                 PPBENLTD
041900                NEXT SENTENCE.                                    PPBENLTD
042000     SKIP1                                                        PPBENLTD
042100*                                                                 PPBENLTD
042200*    ************************************************************ PPBENLTD
042300*    * SAVE PREVIOUS, TO AVOID UNECESSARY DISK ACCESS IN FUTURE * PPBENLTD
042400*    ************************************************************ PPBENLTD
042500*                                                                 PPBENLTD
042600     SKIP1                                                        PPBENLTD
042700     IF FOUND-RATE                                                PPBENLTD
042800         MOVE CTL-SEGMENT-TABLE TO XBRT-BENEFITS-RATES-RECORD     PPBENLTD
042900     ELSE                                                         PPBENLTD
043000         NEXT SENTENCE.                                           PPBENLTD
043100     SKIP3                                                        PPBENLTD
043200 P059-ACCESS-CTL-EXIT.                                            PPBENLTD
043300     EXIT.                                                        PPBENLTD
043400     EJECT                                                        PPBENLTD
043500 R000-SUB-PROGRAM-CALLS SECTION.                                  PPBENLTD
043600     SKIP2                                                        PPBENLTD
043700 R010-CALL-PPBUTUTL.                                              PPBENLTD
043800     SKIP1                                                        PPBENLTD
043900     CALL 'PPBUTUTL' USING PPBUTUTL-INTERFACE.                    PPBENLTD
044000     SKIP3                                                        PPBENLTD
044100 R019-CALL-PPBUTUTL-EXIT.                                         PPBENLTD
044200     EXIT.                                                        PPBENLTD
044300     SKIP3                                                        PPBENLTD
044400 R020-CALL-PPIOCTL.                                               PPBENLTD
044500     SKIP1                                                        PPBENLTD
044600     CALL 'PPIOCTL' USING CTL-INTERFACE                           PPBENLTD
044700                          CTL-SEGMENT-TABLE.                      PPBENLTD
044800     SKIP3                                                        PPBENLTD
044900 R029-CALL-PPIOCTL-EXIT.                                          PPBENLTD
045000     EXIT.                                                        PPBENLTD
