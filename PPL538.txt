000010*==========================================================%      DS413
000020*=    PROGRAM: PPL538                                     =%      DS413
000030*=    CHANGE # DS413        PROJ. REQUEST: IBM OFFLOAD    =%      DS413
000040*=    NAME: R. SWIDERSKI    MODIFICATION DATE: 7/27/89    =%      DS413
000050*=                                                        =%      DS413
000060*=    DESCRIPTION: PROGRAM WAS MODIFIED TO CONFORM        =%      DS413
000070*=    TO COBOL II STANDARDS.                              =%      DS413
000091*==========================================================%      DS413
000092 IDENTIFICATION DIVISION.                                         08/27/86
000093                                                                  PPL538
000094 PROGRAM-ID.                                                         LV014
000095     PPL538.                                                         CL*13
000096                                                                     CL*13
000097 AUTHOR.                                                             CL*13
000098     R SWIDERSKI.                                                    CL*13
000099                                                                     CL*13
000100 INSTALLATION.                                                       CL*13
000200     UCSD ASO.                                                       CL*13
000300                                                                     CL*13
000400 DATE-WRITTEN.                                                       CL*13
000500     MARCH 1982.                                                     CL*13
000600                                                                     CL*13
000700 ENVIRONMENT DIVISION.                                               CL*13
000800                                                                     CL*13
000900 CONFIGURATION SECTION.                                              CL*13
001000 SPECIAL-NAMES.                                                      CL*13
001100     C01 IS TOP-OF-PAGE.                                             CL*13
001200 SOURCE-COMPUTER.                                                    CL*13
001300     IBM-4331.                                                       CL*13
001400                                                                     CL*13
001500 OBJECT-COMPUTER.                                                    CL*13
001600     IBM-4331.                                                       CL*13
001700                                                                     CL*13
001800 INPUT-OUTPUT SECTION.                                               CL*13
001900                                                                     CL*13
002000 FILE-CONTROL.                                                       CL*13
002100                                                                     CL*13
002200     SELECT WORKSTUDY-FILE                                           CL*13
002300         ASSIGN TO VSAM-WRKSTDY                                      CL*13
002400         ORGANIZATION IS INDEXED                                     CL*13
002500         ACCESS MODE IS SEQUENTIAL                                   CL*13
002600         RECORD KEY IS KEY-OF-RECORD                                 CL*13
002700         FILE STATUS IS VSAM-STATUS.                                 CL*13
002800                                                                     CL*13
002900     SELECT WSDIST-REPORT-FILE                                       CL*13
003000         ASSIGN TO UT-S-PRINTFIL.                                    CL*13
003100                                                                     CL*13
003200     COPY CLSLCRPT.                                               UCSD0006
003300                                                                     CL*13
003400 DATA DIVISION.                                                      CL*13
003500                                                                     CL*13
003600 FILE SECTION.                                                       CL*13
003700                                                                     CL*13
003800 FD  WORKSTUDY-FILE                                                  CL*13
003900     LABEL RECORDS ARE STANDARD                                      CL*13
004000     RECORD CONTAINS 60 CHARACTERS                                   CL*13
004100     DATA RECORD IS WORKSTUDY-REC.                                   CL*13
004200 01  WORKSTUDY-REC.                                                  CL*13
004300     05  KEY-OF-RECORD.                                              CL*13
004400         10 EMPLOYEENO           PIC 9(06).                          CL*13
004500         10 WORKSTUDY-ACCT       PIC 9(06).                          CL*13
004600     05  LOCATION                PIC 9(01).                          CL*13
004700     05  RECHARGE-ACCT           PIC 9(06).                          CL*13
004800     05  RECHARGE-FUND           PIC 9(05).                          CL*13
004900     05  RECHARGE-SUB            PIC 9(01).                          CL*13
005000     05  PRES-WORKSTUDY          PIC X(01).                          CL*13
005100     05  END-DATE                PIC 9(06).                          CL*13
005200     05  COMMENTS                PIC X(28).                          CL*13
005300                                                                     CL*13
005400 FD  WSDIST-REPORT-FILE                                              CL*13
005500     RECORDING MODE IS F                                             CL*13
005600     RECORD CONTAINS 133 CHARACTERS                                  CL*13
005700     LABEL RECORDS ARE STANDARD                                      CL*13
005800     DATA RECORD IS WSDIST-REPORT-RECORD.                            CL*13
005900 01  WSDIST-REPORT-RECORD        PIC X(133).                         CL*13
006000                                                                     CL*13
006100     COPY CLFDCRPT.                                               UCSD0006
006200                                                                     CL*13
006300 WORKING-STORAGE SECTION.                                            CL*13
006310*                                                                 DS413
006320 01  DATE-FORMAT.                                                 DS413
006330     COPY CPWSDATE.                                               DS413
006400                                                                     CL*13
006500 77  MAX-LINES-PER-PAGE          PIC 9(2)        VALUE 57.           CL*13
006600                                                                     CL*13
006700 77  MIN-LINES-NECESSARY-FOR-TOTALS                                  CL*13
006800                                 PIC 9(2)        VALUE 51.           CL*13
006900                                                                     CL*13
007000 77  DASH                        PIC X(3)        VALUE '  -'.        CL*13
007100                                                                     CL*13
007200 01  COUNTERS                                    COMP.               CL*13
007300     05  WORKSTUDY-RECORD-CTR    PIC S9(04)      VALUE ZERO.         CL*13
007400     05  EMPLOYEE-CTR            PIC S9(04)      VALUE ZERO.         CL*13
007500     05  NO-EDB-NAME-CTR         PIC S9(04)      VALUE ZERO.         CL*13
007600     05  LINE-CTR                PIC S9(04)      VALUE ZERO.         CL*13
007700     05  PAGE-CTR                PIC S9(04)      VALUE ZERO.         CL*13
007800                                                                     CL*13
007900 01  VSAM-STATUS.                                                    CL*13
008000     05  VSAM-STATUS-BYTE-1      PIC X.                              CL*13
008100     05  VSAM-STATUS-BYTE-2      PIC X.                              CL*13
008200                                                                     CL*13
008300 01  HOLD-DATE.                                                      CL*13
008400     05  HOLD-YEAR               PIC 9(2)        VALUE ZEROS.        CL*13
008500     05  HOLD-MONTH              PIC 9(2)        VALUE ZEROS.        CL*13
008600     05  HOLD-DAY                PIC 9(2)        VALUE ZEROS.        CL*13
008700                                                                     CL*13
008800 01  DATE-FORMATTED.                                                 CL*13
008900     05  MONTH-FORMAT            PIC X(2)        VALUE SPACES.       CL*13
009000     05  FILLER                  PIC X(1)        VALUE '/'.          CL*13
009100     05  DAY-FORMAT              PIC X(2)        VALUE SPACES.       CL*13
009200     05  FILLER                  PIC X(1)        VALUE '/'.          CL*13
009300     05  YEAR-FORMAT             PIC X(2)        VALUE SPACES.       CL*13
009400                                                                     CL*13
009500 01  WORKSTUDY-FILE-EOF-FLAG     PIC X(3)        VALUE 'NO '.        CL*13
009600     88  AT-END-OF-WORKSTUDY-FILE                VALUE 'YES'.        CL*13
009700                                                                     CL*13
009800 01  EMP-ID-9CHAR.                                                   CL*13
009900     05  FILLER                  PIC X(3)        VALUE ZEROS.        CL*13
010000     05  LAST-EMPLOYEE-READ      PIC X(6)        VALUE ZEROS.        CL*13
010100                                                                     CL*13
010200 01  PRINTABLE-LINES-REMAINING   PIC 9(2)        VALUE ZEROS.        CL*13
010300                                                                     CL*13
010400 01  HDG-LINE-1.                                                     CL*13
010500     05  CARR-CTL-CHAR           PIC X(1)        VALUE SPACE.        CL*13
010600     05  FILLER                  PIC X(28)       VALUE SPACES.       CL*13
010700     05  FILLER                  PIC X(28)       VALUE               CL*13
010800         'WORK-STUDY DISTRIBUTION FILE'.                             CL*13
010900     05  FILLER                  PIC X(9)        VALUE SPACES.       CL*13
011000     05  DATE-HDG.                                                   CL*13
011100         10  MONTH-HDG           PIC 9(2)        VALUE ZEROS.        CL*13
011200         10  FILLER              PIC X(1)        VALUE '/'.          CL*13
011300         10  DAY-HDG             PIC 9(2)        VALUE ZEROS.        CL*13
011400         10  FILLER              PIC X(1)        VALUE '/'.          CL*13
011500         10  YEAR-HDG            PIC 9(2)        VALUE ZEROS.        CL*13
011600     05  FILLER                  PIC X(3)        VALUE SPACES.       CL*13
011700     05  FILLER                  PIC X(4)        VALUE 'PAGE'.       CL*13
011800     05  FILLER                  PIC X(1)        VALUE SPACE.        CL*13
011900     05  PAGE-NO                 PIC Z(3)9(1).                       CL*13
012000     05  FILLER                  PIC X(47)       VALUE SPACES.       CL*13
012100                                                                     CL*13
012200 01  HDG-LINE-2.                                                     CL*13
012300     05  CARR-CTL-CHAR           PIC X(1)        VALUE SPACE.        CL*13
012400     05  FILLER                  PIC X(33)       VALUE SPACES.       CL*13
012500     05  FILLER                  PIC X(2)        VALUE 'ID'.         CL*13
012600     05  FILLER                  PIC X(5)        VALUE SPACES.       CL*13
012700     05  FILLER                  PIC X(10)       VALUE               CL*13
012800         'WORK-STUDY'.                                               CL*13
012900     05  FILLER                  PIC X(82)       VALUE SPACES.       CL*13
013000                                                                     CL*13
013100 01  HDG-LINE-3.                                                     CL*13
013200     05  CARR-CTL-CHAR           PIC X(1)        VALUE SPACE.        CL*13
013300     05  FILLER                  PIC X(13)       VALUE               CL*13
013400         'EMPLOYEE NAME'.                                            CL*13
013500     05  FILLER                  PIC X(18)       VALUE SPACES.       CL*13
013600     05  FILLER                  PIC X(6)        VALUE 'NUMBER'.     CL*13
013700     05  FILLER                  PIC X(4)        VALUE SPACES.       CL*13
013800     05  FILLER                  PIC X(7)        VALUE 'ACCOUNT'.    CL*13
013900     05  FILLER                  PIC X(3)        VALUE SPACES.       CL*13
014000     05  FILLER                  PIC X(16)       VALUE               CL*13
014100         'MATCHING ACCOUNT'.                                         CL*13
014200     05  FILLER                  PIC X(2)        VALUE SPACES.       CL*13
014300     05  FILLER                  PIC X(4)        VALUE 'PRES'.       CL*13
014400     05  FILLER                  PIC X(5)        VALUE SPACES.       CL*13
014500     05  FILLER                  PIC X(4)        VALUE 'DATE'.       CL*13
014600     05  FILLER                  PIC X(50)       VALUE SPACES.       CL*13
014700                                                                     CL*13
014800 01  HDG-LINE-4.                                                     CL*13
014900     05  FILLER                  PIC X(1)        VALUE SPACE.        CL*13
015000     05  FILLER                  PIC X(13)       VALUE               CL*13
015100         '-------------'.                                            CL*13
015200     05  FILLER                  PIC X(18)       VALUE SPACES.       CL*13
015300     05  FILLER                  PIC X(6)        VALUE '------'.     CL*13
015400     05  FILLER                  PIC X(4)        VALUE SPACES.       CL*13
015500     05  FILLER                  PIC X(7)        VALUE '-------'.    CL*13
015600     05  FILLER                  PIC X(3)        VALUE SPACES.       CL*13
015700     05  FILLER                  PIC X(16)       VALUE               CL*13
015800         '----------------'.                                         CL*13
015900     05  FILLER                  PIC X(2)        VALUE SPACES.       CL*13
016000     05  FILLER                  PIC X(4)        VALUE '----'.       CL*13
016100     05  FILLER                  PIC X(5)        VALUE SPACES.       CL*13
016200     05  FILLER                  PIC X(4)        VALUE '----'.       CL*13
016300     05  FILLER                  PIC X(50)       VALUE SPACES.       CL*13
016400                                                                     CL*13
016500 01  DETAIL-LINE.                                                    CL*13
016600     05  FILLER                  PIC X(1)        VALUE SPACE.        CL*13
016700     05  EMP-NAME                PIC X(28)       VALUE SPACES.       CL*13
016800     05  FILLER                  PIC X(3)        VALUE SPACES.       CL*13
016900     05  EMPLOYEENO              PIC X(6)        VALUE SPACES.       CL*13
017000     05  FILLER                  PIC X(5)        VALUE SPACES.       CL*13
017100     05  WORKSTUDY-ACCT          PIC X(6)        VALUE SPACES.       CL*13
017200     05  FILLER                  PIC X(5)        VALUE SPACES.       CL*13
017300     05  RECHARGE-ACCT           PIC X(6)        VALUE SPACES.       CL*13
017400     05  FILLER                  PIC X(1)        VALUE SPACE.        CL*13
017500     05  RECHARGE-FUND           PIC X(5)        VALUE SPACES.       CL*13
017600     05  FILLER                  PIC X(5)        VALUE SPACES.       CL*13
017700     05  PRES-WORKSTUDY          PIC X(1)        VALUE SPACE.        CL*13
017800     05  FILLER                  PIC X(5)        VALUE SPACES.       CL*13
017900     05  ENDING-DATE             PIC X(8)        VALUE SPACES.       CL*13
018000     05  FILLER                  PIC X(48)       VALUE SPACES.       CL*13
018100                                                                     CL*13
018200 01  FOOTING-LINE-1.                                                 CL*13
018300     05  CARR-CTL-CHAR           PIC X(1)        VALUE SPACE.        CL*13
018400     05  FILLER                  PIC X(28)       VALUE               CL*13
018500         'WORK-STUDY RECORDS = '.                                    CL*13
018600     05  FILLER                  PIC X(1)        VALUE SPACE.        CL*13
018700     05  WORKSTUDY-REC-TOTAL     PIC Z(2),Z(2)9(1).                  CL*13
018800     05  FILLER                  PIC X(97)       VALUE SPACES.       CL*13
018900                                                                     CL*13
019000 01  FOOTING-LINE-2.                                                 CL*13
019100     05  CARR-CTL-CHAR           PIC X(1)        VALUE SPACE.        CL*13
019200     05  FILLER                  PIC X(28)       VALUE               CL*13
019300         'INDIVIDUALS = '.                                           CL*13
019400     05  FILLER                  PIC X(1)        VALUE SPACE.        CL*13
019500     05  EMPLOYEE-TOTAL          PIC Z(2),Z(2)9(1).                  CL*13
019600     05  FILLER                  PIC X(97)       VALUE SPACES.       CL*13
019700                                                                     CL*13
019800 01  FOOTING-LINE-3.                                                 CL*13
019900     05  CARR-CTL-CHAR           PIC X(1)        VALUE SPACE.        CL*13
020000     05  FILLER                  PIC X(28)       VALUE               CL*13
020100         'INDIVIDUALS MISSING NAMES = '.                             CL*13
020200     05  FILLER                  PIC X(1)        VALUE SPACE.        CL*13
020300     05  NO-EDB-NAME-TOTAL       PIC Z(2),Z(2)9(1).                  CL*13
020400     05  FILLER                  PIC X(97)       VALUE SPACES.       CL*13
020500                                                                     CL*13
020510 01  IO-FUNCTIONS.                                                DS413
020600            COPY CPWSXIOF.                                           CL*13
020700                                                                     CL*13
020710 01  XCFK-CONTROL-FILE-KEYS.                                      DS413
020800            COPY CPWSXCFK.                                           CL*13
020900                                                                     CL*13
020910 01  EDB-INTERFACE.                                               DS413
021000            COPY CPWSXEIF.                                           CL*13
021100                                                                     CL*13
021110 01  EDB-SEGMENT-TABLE.                                           DS413
021200            COPY CPWSXEST.                                           CL*13
021300                                                                     CL*13
021310 01  XDBS-PERSONAL-DATA1.                                         DS413
021400            COPY CPWSDB01.                                           CL*13
021500                                                                     CL*13
021600     COPY CLWSCRPT.                                               UCSD0006
021700                                                                     CL*13
021800 PROCEDURE DIVISION.                                                 CL*13
021900                                                                     CL*13
022000 MAIN-PARA.                                                          CL*13
022100     COPY CLPDCRPT.                                               UCSD0006
022200     OPEN  INPUT WORKSTUDY-FILE                                      CL*13
022300          OUTPUT WSDIST-REPORT-FILE.                                 CL*13
022400                                                                     CL*13
022500     ACCEPT HOLD-DATE FROM DATE.                                     CL*13
022600     MOVE HOLD-MONTH TO MONTH-HDG.                                   CL*13
022700     MOVE HOLD-DAY TO DAY-HDG.                                       CL*13
022800     MOVE HOLD-YEAR TO YEAR-HDG.                                     CL*13
022900     PERFORM WRITE-REPORT-HDG.                                       CL*13
023000     PERFORM PREPARE-EDB-FOR-ACCESS.                                 CL*13
023100     MOVE ZEROS TO KEY-OF-RECORD.                                    CL*13
023200     PERFORM READ-WORKSTUDY-FILE.                                    CL*13
023300     PERFORM PROCESS-WORKSTUDY-FILE                                  CL*13
023400         UNTIL AT-END-OF-WORKSTUDY-FILE.                             CL*13
023500     PERFORM CLOSE-EDB.                                              CL*13
023600     PERFORM WRITE-RECORD-COUNTS.                                    CL*13
023700     PERFORM EOJ-ROUTINE.                                            CL*13
023800                                                                     CL*13
023900 EOJ-ROUTINE.                                                        CL*13
024000                                                                     CL*13
024100     PERFORM PRINT-CONTROL-REPORT.                                UCSD0006
024200     CLOSE WORKSTUDY-FILE                                            CL*13
024300           WSDIST-REPORT-FILE.                                       CL*13
024400                                                                     CL*13
024500     STOP RUN.                                                       CL*13
024600                                                                     CL*13
024700 PREPARE-EDB-FOR-ACCESS.                                             CL*13
024800                                                                     CL*13
024900     MOVE RAND-OPEN-IN TO IO-EDB-ACTION.                             CL*13
025000     PERFORM READ-EDB.                                               CL*13
025100     MOVE RAND-READ TO IO-EDB-ACTION.                                CL*13
025200     MOVE '0100' TO IO-EDB-NOM-SEG.                                  CL*13
025300                                                                     CL*13
025400 PROCESS-WORKSTUDY-FILE.                                             CL*13
025500                                                                     CL*13
025600     ADD 1 TO LINE-CTR                                               CL*13
025700              WORKSTUDY-RECORD-CTR.                                  CL*13
025800     IF LINE-CTR > MAX-LINES-PER-PAGE                                CL*13
025900         PERFORM WRITE-REPORT-HDG                                    CL*13
026000         ADD 1 TO LINE-CTR.                                          CL*13
026100     IF EMPLOYEENO OF WORKSTUDY-REC NOT = LAST-EMPLOYEE-READ         CL*13
026200         AND LINE-CTR > 7                                            CL*13
026300             MOVE SPACES TO WSDIST-REPORT-RECORD                     CL*13
026400             PERFORM WRITE-WSDIST-REPORT-RECORD                      CL*13
026500             ADD 1 TO LINE-CTR.                                      CL*13
026600     IF EMPLOYEENO OF WORKSTUDY-REC NOT = LAST-EMPLOYEE-READ         CL*13
026700         ADD 1 TO EMPLOYEE-CTR                                       CL*13
026800         MOVE EMPLOYEENO OF WORKSTUDY-REC TO                         CL*13
026900             EMPLOYEENO OF DETAIL-LINE                               CL*13
027000             LAST-EMPLOYEE-READ                                      CL*13
027100         MOVE EMP-ID-9CHAR TO IO-EDB-NOM-SS-NUM                      CL*13
027200         PERFORM GET-NAME-FROM-EDB                                   CL*13
027300     ELSE                                                            CL*13
027400         MOVE DASH TO EMP-NAME                                       CL*13
027500                      EMPLOYEENO OF DETAIL-LINE.                     CL*13
027600     MOVE WORKSTUDY-ACCT OF WORKSTUDY-REC TO                         CL*13
027700         WORKSTUDY-ACCT OF DETAIL-LINE.                              CL*13
027800     MOVE RECHARGE-ACCT OF WORKSTUDY-REC TO                          CL*13
027900         RECHARGE-ACCT OF DETAIL-LINE.                               CL*13
028000     MOVE RECHARGE-FUND OF WORKSTUDY-REC TO                          CL*13
028100         RECHARGE-FUND OF DETAIL-LINE.                               CL*13
028200     MOVE PRES-WORKSTUDY OF WORKSTUDY-REC TO                         CL*13
028300         PRES-WORKSTUDY OF DETAIL-LINE.                              CL*13
028400     MOVE END-DATE TO HOLD-DATE.                                     CL*13
028500     MOVE HOLD-DAY TO DAY-FORMAT.                                    CL*13
028600     MOVE HOLD-MONTH TO MONTH-FORMAT.                                CL*13
028700     MOVE HOLD-YEAR TO YEAR-FORMAT.                                  CL*13
028800     MOVE DATE-FORMATTED TO ENDING-DATE.                             CL*13
028900     MOVE DETAIL-LINE TO WSDIST-REPORT-RECORD.                       CL*13
029000     PERFORM WRITE-WSDIST-REPORT-RECORD.                             CL*13
029100     PERFORM READ-WORKSTUDY-FILE.                                    CL*13
029200                                                                     CL*13
029300 WRITE-REPORT-HDG.                                                   CL*13
029400     ADD 1 TO PAGE-CTR.                                              CL*13
029500     MOVE PAGE-CTR TO PAGE-NO.                                       CL*13
029600     WRITE WSDIST-REPORT-RECORD FROM HDG-LINE-1                      CL*13
029700         AFTER ADVANCING PAGE.                                       CL*13
029800     MOVE SPACES TO WSDIST-REPORT-RECORD.                            CL*13
029900     PERFORM WRITE-WSDIST-REPORT-RECORD.                             CL*13
030000     MOVE HDG-LINE-2 TO WSDIST-REPORT-RECORD.                        CL*13
030100     PERFORM WRITE-WSDIST-REPORT-RECORD.                             CL*13
030200     MOVE HDG-LINE-3 TO WSDIST-REPORT-RECORD.                        CL*13
030300     PERFORM WRITE-WSDIST-REPORT-RECORD.                             CL*13
030400     MOVE HDG-LINE-4 TO WSDIST-REPORT-RECORD.                        CL*13
030500     PERFORM WRITE-WSDIST-REPORT-RECORD.                             CL*13
030600     MOVE SPACES TO WSDIST-REPORT-RECORD.                            CL*13
030700     PERFORM WRITE-WSDIST-REPORT-RECORD.                             CL*13
030800     MOVE 6 TO LINE-CTR.                                             CL*13
030900                                                                     CL*13
031000 GET-NAME-FROM-EDB.                                                  CL*13
031100     PERFORM READ-EDB.                                               CL*13
031200     IF SEG-ID (1) = EMP-ID-9CHAR                                    CL*13
031300*        MOVE EDB-SEG-TAB (1) TO XDBS-0100-SEGMENT                   CL*13
031400         MOVE EDB-SEG-TAB (1) TO XDBS-PERSONAL-DATA1                 CL*13
031500         MOVE XDBS-0105-NAME TO EMP-NAME                             CL*13
031600     ELSE                                                            CL*13
031700         ADD 1 TO NO-EDB-NAME-CTR                                    CL*13
031800         MOVE '*** WARNING - NO NAME ON EDB' TO EMP-NAME.            CL*13
031900                                                                     CL*13
032000 READ-EDB.                                                           CL*13
032100     CALL 'PPIOEDB'                                                  CL*13
032200         USING EDB-INTERFACE                                         CL*13
032300               EDB-SEGMENT-TABLE.                                    CL*13
032400                                                                     CL*13
032500 READ-WORKSTUDY-FILE.                                                CL*13
032600                                                                     CL*13
032700     READ WORKSTUDY-FILE                                             CL*13
032800         AT END                                                      CL*13
032900             MOVE 'YES' TO WORKSTUDY-FILE-EOF-FLAG.                  CL*13
033000                                                                     CL*13
033100 CLOSE-EDB.                                                          CL*13
033200     MOVE RAND-CLOSE TO IO-EDB-ACTION.                               CL*13
033300     PERFORM READ-EDB.                                               CL*13
033400                                                                     CL*13
033500 WRITE-RECORD-COUNTS.                                                CL*13
033600     MOVE WORKSTUDY-RECORD-CTR TO WORKSTUDY-REC-TOTAL.               CL*13
033700     SUBTRACT LINE-CTR FROM MAX-LINES-PER-PAGE                       CL*13
033800         GIVING PRINTABLE-LINES-REMAINING.                           CL*13
033900     IF PRINTABLE-LINES-REMAINING NOT <                              CL*13
034000         MIN-LINES-NECESSARY-FOR-TOTALS                              CL*13
034100             WRITE WSDIST-REPORT-RECORD FROM FOOTING-LINE-1          CL*13
034200                 AFTER ADVANCING PAGE                                CL*13
034300     ELSE                                                            CL*13
034400             MOVE SPACES TO WSDIST-REPORT-RECORD                     CL*13
034500             PERFORM WRITE-WSDIST-REPORT-RECORD 3 TIMES              CL*13
034600             MOVE FOOTING-LINE-1 TO WSDIST-REPORT-RECORD             CL*13
034700             PERFORM WRITE-WSDIST-REPORT-RECORD.                     CL*13
034800     MOVE SPACES TO WSDIST-REPORT-RECORD.                            CL*13
034900     PERFORM WRITE-WSDIST-REPORT-RECORD.                             CL*13
035000     MOVE EMPLOYEE-CTR TO EMPLOYEE-TOTAL.                            CL*13
035100     MOVE FOOTING-LINE-2 TO WSDIST-REPORT-RECORD.                    CL*13
035200     PERFORM WRITE-WSDIST-REPORT-RECORD.                             CL*13
035300     MOVE SPACES TO WSDIST-REPORT-RECORD.                            CL*13
035400     PERFORM WRITE-WSDIST-REPORT-RECORD.                             CL*13
035500     MOVE NO-EDB-NAME-CTR TO NO-EDB-NAME-TOTAL.                      CL*13
035600     MOVE FOOTING-LINE-3 TO WSDIST-REPORT-RECORD.                    CL*13
035700     PERFORM WRITE-WSDIST-REPORT-RECORD.                             CL*13
035800                                                                     CL*13
035900 WRITE-WSDIST-REPORT-RECORD.                                         CL*13
036000     WRITE WSDIST-REPORT-RECORD.                                     CL*13
036100*                                                                 08/12/86
036200*      CONTROL REPORT PPL538CR                                    CRPDL538
036300*                                                                    LV002
036400*                                                                 CRPDL538
036500 PRINT-CONTROL-REPORT.                                            CRPDL538
036600                                                                  CRPDL538
036700     OPEN OUTPUT CONTROLREPORT.                                      CL**2
036800                                                                  CRPDL538
036900     MOVE 'PPL538CR'             TO CR-HL1-RPT.                   CRPDL538
037000     MOVE CR-HDG-LINE1           TO CONTROL-DATA.                 CRPDL538
037100     WRITE CONTROLRECORD AFTER ADVANCING PAGE.                       CL**2
037200                                                                  CRPDL538
037300     MOVE CR-HDG-LINE2           TO CONTROL-DATA.                 CRPDL538
037400     WRITE CONTROLRECORD AFTER ADVANCING 1 LINES.                    CL**2
037500                                                                  CRPDL538
037600     MOVE WHEN-COMPILED          TO COMPILE-TIME-AND-DATE.        CRPDL538
037700     MOVE COMPILE-TIME           TO CR-HL3-CMP-TIME.              CRPDL538
037800     MOVE COMPILE-DATE           TO CR-HL3-CMP-DATE.              CRPDL538
037900     MOVE CR-HDG-LINE3           TO CONTROL-DATA.                 CRPDL538
038000     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                    CL**2
038100                                                                  CRPDL538
038200     MOVE CR-HDG-LINE4           TO CONTROL-DATA.                 CRPDL538
038300     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                    CL**2
038400                                                                  CRPDL538
038500     MOVE NO-CONTROL-CARD        TO CR-DL5-CTL-CARD.              CRPDL538
038600     MOVE CR-DET-LINE5           TO CONTROL-DATA.                 CRPDL538
038700     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                    CL**2
038800                                                                  CRPDL538
038900     MOVE CR-HDG-LINE6           TO CONTROL-DATA.                 CRPDL538
039000     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                    CL**2
039100                                                                  CRPDL538
039200     MOVE TIME-OF-DAY            TO TIME-WORK-AREA.               CRPDL538
039210*    ACCEPT TIME-WORK-AREA FROM TIME.                             DS413 38
039300     MOVE CORR TIME-WORK-AREA    TO TIME-WORK-AREA-SPLIT.         CRPDL538
039400     MOVE TIME-WORK-AREA-SPLIT   TO CR-DL7-CTL-TIME.              CRPDL538
039500     MOVE CR-DET-LINE7           TO CONTROL-DATA.                 CRPDL538
039600     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                    CL**2
039700                                                                  CRPDL538
039800     MOVE CR-HDG-LINE8           TO CONTROL-DATA.                 CRPDL538
039900     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                    CL**2
040000*                                                                 CRPDL538
040100*  FOLLOWING CODE MUST BE DUPLICATED FOR EVERY FILE               CRPDL538
040200*                                                                 CRPDL538
040300*  FILE NAME, ACTION (SO-CREATED OR SO-READ), AND                 CRPDL538
040400*  THE ACTUAL RECORD COUNTER MUST BE FILLED IN                    CRPDL538
040500*                                                                 CRPDL538
040600     MOVE 'WRKSTYV '             TO CR-DL9-FILE.                  CRPDL538
040700     MOVE SO-READ                TO CR-DL9-ACTION.                CRPDL538
040800     MOVE WORKSTUDY-RECORD-CTR   TO CR-DL9-VALUE.                 CRPDL538
040900     MOVE CR-DET-LINE9           TO CONTROL-DATA.                 CRPDL538
041000     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                    CL**2
041100*                                                                 CRPDL538
041200*                                                                 CRPDL538
041300*                                                                 CRPDL538
041400     MOVE END-OF-REPORT-MSG      TO CONTROL-DATA.                 CRPDL538
041500     WRITE CONTROLRECORD AFTER ADVANCING 4 LINES.                    CL**2
041600                                                                  CRPDL538
041700     CLOSE CONTROLREPORT.                                            CL**2
